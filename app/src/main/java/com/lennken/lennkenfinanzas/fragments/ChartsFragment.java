package com.lennken.lennkenfinanzas.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lennken.lennkenfinanzas.R;

import butterknife.ButterKnife;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
public class ChartsFragment extends BaseFragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_charts, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(this, getView());
    }
}
