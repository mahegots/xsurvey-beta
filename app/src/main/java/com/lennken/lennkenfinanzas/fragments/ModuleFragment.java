package com.lennken.lennkenfinanzas.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.activities.MainActivity;
import com.lennken.lennkenfinanzas.adapters.VisitAdapter;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.Module;
import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.TemplateSchema;
import com.lennken.lennkenfinanzas.db.models.Visit;
import com.lennken.lennkenfinanzas.db.models.WorkPlan;
import com.lennken.lennkenfinanzas.events.logic.EventLaunchSchema;
import com.lennken.lennkenfinanzas.events.logic.EventSchemaDownload;
import com.lennken.lennkenfinanzas.events.logic.EventWorkPlanDownloaded;
import com.lennken.lennkenfinanzas.events.ui.EventCallClicked;
import com.lennken.lennkenfinanzas.events.ui.EventNavigationClick;
import com.lennken.lennkenfinanzas.events.ui.EventVisitDetailClicked;
import com.lennken.lennkenfinanzas.tasks.SchemaDownloadTask;
import com.lennken.lennkenfinanzas.tasks.WorkPlanDownloadTask;
import com.lennken.lennkenfinanzas.utils.IntentUtils;
import com.lennken.lennkenfinanzas.xsurvey.activities.SurveyListActivity;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.SyncEvents;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import io.realm.Realm;
import io.realm.RealmList;
import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
@SuppressLint("ValidFragment")
public class ModuleFragment extends BaseFragment {

    private final String moduleName;
    @Bind(R.id.fr_prospecting_list)
    ExpandableStickyListHeadersListView mListHeadersListView;
    @Bind(R.id.empty)
    TextView emptyTextView;
    @Bind(R.id.fr_module_add)
    FloatingActionButton mAddButton;
    private VisitAdapter mVisitAdapter;
    private WorkPlan mWorkPlan;
    private Module module;
    private Visit mVisitSelected;

    public ModuleFragment(String moduleName) {
        this.moduleName = moduleName;
    }

    public static ModuleFragment newInstance(String moduleName) {
        return new ModuleFragment(moduleName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_module, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(this, getView());
        mWorkPlan = Dao.workPlan(mContext);
        mVisitAdapter = new VisitAdapter(mContext, new RealmList<Visit>());
        mListHeadersListView.setAdapter(mVisitAdapter);
//        if (mWorkPlan == null) {
            WorkPlanDownloadTask task = new WorkPlanDownloadTask(mContext);
            task.execute();
//        } else {
           // loadModule();
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ((MainActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
    }

    private void loadModule() {
        module = Dao.module(mContext, moduleName);
        if (module == null) {
            Snackbar.make(getView(), getString(R.string.error_module, moduleName),
                    Snackbar.LENGTH_LONG).show();
        } else {
            RealmList<Visit> visits = Dao.visits(mContext, moduleName);
            RealmList<Visit> visitsDone = Dao.visitsDone(mContext, moduleName);

            String title = "";
            switch (moduleName) {
                case "originacion":
                    title = getString(R.string.prospecting);
                    break;
                case "preaprobacion":
                    title = getString(R.string.menu_pre_approval);
                    break;
                case "cobranza":
                    title = getString(R.string.menu_billing);
                    break;
                case "encuesta":
                    title = getString(R.string.menu_survey);
                    break;
                default:
                    break;
            }
//            String title = moduleName.equals("originacion")
//                    ? getString(R.string.prospecting) : getString(R.string.menu_billing);
            getActivity().setTitle(title + " (" + visitsDone.size() + "/" + visits.size() + ")");
            updateView(visits.size());
            mVisitAdapter.setVisits(visits);
            mVisitAdapter.notifyDataSetChanged();
        }
    }

    private void updateView(int size) {
        mListHeadersListView.setVisibility(size == 0 ? View.GONE : View.VISIBLE);
        emptyTextView.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    @Subscribe
    public void onWorkPlanEvent(EventWorkPlanDownloaded event) {
        mWorkPlan = event.workPlan;
        if (mWorkPlan == null) {
            //Log.d(getClass().getName(), event.error);
            Snackbar.make(getView(), R.string.error_downloading_workplan, Snackbar.LENGTH_LONG).show();
        } else {
            Dao.saveWorkPlan(mContext, mWorkPlan);
            loadModule();
        }
    }

    @Subscribe
    public void onCallEvent(EventCallClicked event) {
        IntentUtils.callPhone(mContext, event.visit != null ? event.visit.getTelephoneNumber() : "");
    }

    @Subscribe
    public void onNavigateEvent(EventNavigationClick event) {
        IntentUtils.navigateTo(mContext, event.visit != null ? event.visit.getAddressOne() : "");
    }

    @Subscribe
    public void onDetailEvent(EventVisitDetailClicked event) {
        int schemaVersion = module.getSchemaVersion();
        mVisitSelected = event.visit;

        Schema schema = null;
        if (mVisitSelected.getIdVisit() != -1) {
            schema = Dao.schema(mContext, schemaVersion, mVisitSelected.getIdVisit());
        }
        if (schema == null) {
            schema = new TemplateSchema.Builder(mContext).newSchema(schemaVersion);
            if (schema == null) {
                Log.d(getClass().getName(), "Schema not downloaded");
                SchemaDownloadTask task = new SchemaDownloadTask(mContext, schemaVersion);
                task.execute();
            } else {
                loadSchema(schema);
            }
        } else {
            loadSchema(schema);
        }
    }

    @Subscribe
    public void onSchemaDownloadEvent(EventSchemaDownload event) {
        Log.d(getClass().getName(), "Downloaded Schema");
        if (event.schema != null) {
            TemplateSchema templateSchema = new TemplateSchema();
            templateSchema.setSchemaVersion(module.getSchemaVersion());
            templateSchema.setSchema(event.schema);
            Dao.insert(mContext, templateSchema);
            loadSchema(event.schema);
        } else {
            Snackbar.make(getView(), R.string.error_downloading_schema, Snackbar.LENGTH_LONG).show();
        }
    }

    private void loadSchema(Schema schema) {
        Realm realm = Realm.getInstance(mContext);
        realm.beginTransaction();
        schema.setIdVisit(mVisitSelected.getIdVisit());
        schema.setIdModule(module.getIdModule());
        schema.setIdWorkPlan(mWorkPlan.getId());
        schema.setSchemaVersion(module.getSchemaVersion());
        schema.setVisit(mVisitSelected);
        schema = realm.copyToRealmOrUpdate(schema);
        realm.commitTransaction();
        Intent intent = new Intent(mContext, SurveyListActivity.class);
        mBus.postSticky(new EventLaunchSchema(schema, mWorkPlan));
        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onSyncStartEvent(SyncEvents.SyncStartEvent ignored) {
        ((MainActivity) getActivity()).getProgressBar().setVisibility(View.VISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onSyncEvent(SyncEvents.SyncFinishEvent ignored) {
        ((MainActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
        loadModule();
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onSyncErrorEvent(SyncEvents.SyncErrorEvent event) {
        Toast.makeText(mContext, event.error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.fr_module_add)
    void onAddClick() {
        try {
            Visit visit = new Visit();
            visit.setIdVisit(-1);
            visit = Dao.insert(mContext, visit);
            Realm realm = Realm.getInstance(mContext);
            realm.beginTransaction();
            module.getVisits().add(visit);
            module = realm.copyToRealmOrUpdate(module);
            realm.commitTransaction();
            onDetailEvent(new EventVisitDetailClicked(visit));
        }catch(NullPointerException ex){
            ex.printStackTrace();
            Toast.makeText(getActivity(), "No hay modulos asignados", Toast.LENGTH_SHORT).show();
        }catch(IllegalStateException e){
            e.printStackTrace();
            Toast.makeText(getActivity(), "No hay registros", Toast.LENGTH_SHORT).show();;
        }
    }
}
