package com.lennken.lennkenfinanzas.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.adapters.CompletedTabAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class CompletedFragment extends BaseFragment{

    @Bind(R.id.tab_layout)
    TabLayout mTabLayout;
    @Bind(R.id.fr_billing_main_pager)
    ViewPager mViewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_billing_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(this, getView());
        setupTabs();
    }

    private void setupTabs() {
        mViewPager.setAdapter(new CompletedTabAdapter(getChildFragmentManager()));
        mTabLayout.addTab(mTabLayout.newTab().setText("Originación"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Preaprobación"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Cobranza"));
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
