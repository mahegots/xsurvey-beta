package com.lennken.lennkenfinanzas.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.adapters.VisitDoneAdapter;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.Module;
import com.lennken.lennkenfinanzas.db.models.Visit;
import com.lennken.lennkenfinanzas.db.models.WorkPlan;
import com.lennken.lennkenfinanzas.events.logic.EventWorkPlanDownloaded;
import com.lennken.lennkenfinanzas.tasks.WorkPlanDownloadTask;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.SyncEvents;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import io.realm.RealmList;
import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
@SuppressLint("ValidFragment")
public class ModuleCompletedFragment extends BaseFragment {

    @Bind(R.id.fr_prospecting_list)
    ExpandableStickyListHeadersListView mListHeadersListView;
    @Bind(R.id.empty)
    TextView emptyTextView;
    @Bind(R.id.fr_module_add)
    FloatingActionButton mAddButton;

    private VisitDoneAdapter mVisitAdapter;
    private WorkPlan mWorkPlan;
    private String moduleName;

    public ModuleCompletedFragment(String moduleName) {
        this.moduleName = moduleName;
    }

    public static ModuleCompletedFragment newInstance(String moduleName){
        return new ModuleCompletedFragment(moduleName);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_module, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(this, getView());
        mAddButton.setVisibility(View.GONE);
        mWorkPlan = Dao.workPlan(mContext);
        mVisitAdapter = new VisitDoneAdapter(mContext, new RealmList<Visit>());
        mListHeadersListView.setAdapter(mVisitAdapter);
        if (mWorkPlan == null) {
            WorkPlanDownloadTask task = new WorkPlanDownloadTask(mContext);
            task.execute();
        } else {
            loadModule();
        }
    }

    private void loadModule() {
        Module mBillingModule = Dao.module(mContext, moduleName);
        if (mBillingModule == null) {
            Snackbar.make(getView(), R.string.error_billing, Snackbar.LENGTH_LONG).show();
        } else {
            RealmList<Visit> visits = Dao.visitsDone(mContext, moduleName);
            updateView(visits.size());
            mVisitAdapter.setVisits(visits);
            mVisitAdapter.notifyDataSetChanged();

        }
    }

    private void updateView(int size) {
        mListHeadersListView.setVisibility(size == 0 ? View.GONE : View.VISIBLE);
        emptyTextView.setVisibility(size == 0 ? View.VISIBLE : View.GONE);
    }

    @Subscribe
    public void onWorkPlanEvent(EventWorkPlanDownloaded event) {
        mWorkPlan = event.workPlan;
        if (mWorkPlan == null) {
            Log.d(getClass().getName(), event.error);
            Snackbar.make(getView(), R.string.error_downloading_workplan, Snackbar.LENGTH_LONG).show();
        } else {
            Dao.saveWorkPlan(mContext, mWorkPlan);
            loadModule();
        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onSyncEvent(SyncEvents.SyncFinishEvent ignored){
        loadModule();
    }

}
