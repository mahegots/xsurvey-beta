package com.lennken.lennkenfinanzas.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;

/**
 * Created by Daniel García Alvarado on 7/10/15.
 * Lennken Finanzas - angelgomez
 */
public class AboutDialogFragment extends DialogFragment {

    public static AboutDialogFragment newInstance() {
        return new AboutDialogFragment();
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_about_dialog, null);

        mBuilder.setView(view);
        mBuilder.setTitle(getResources().getString(R.string.about));

        TextView mVersionTextView =
                (TextView) view.findViewById(R.id.fragment_about_dialog_version);

        PackageInfo mPackageInfo;
        try {
            mPackageInfo = getActivity()
                    .getPackageManager()
                    .getPackageInfo(getActivity().getPackageName(), 0);
            String mVersion = mPackageInfo.versionName;
            mVersionTextView.setText("v. " + mVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        mBuilder.setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return mBuilder.create();
    }

}
