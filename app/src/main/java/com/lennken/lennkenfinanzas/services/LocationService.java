/*
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennken.lennkenfinanzas.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.lennken.lennkenfinanzas.BuildConfig;
import com.lennken.lennkenfinanzas.LnkApp;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.User;
import com.lennken.lennkenfinanzas.events.logic.EventLocationChanged;
import com.lennken.lennkenfinanzas.utils.Constants;
import com.lennken.lennkenfinanzas.ws.requests.DeviceRequest;
import com.lennken.lennkenfinanzas.ws.responses.DeviceResponse;

import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.google.android.gms.location.LocationServices.FusedLocationApi;

/**
 * A utility IntentService, used for a variety of asynchronous background
 * operations that do not necessarily need to be tied to a UI.
 */
public class LocationService extends IntentService {
    private static final String TAG = LocationService.class.getSimpleName();

    private static final String ACTION_LOCATION_UPDATED = "location_updated";
    private static final String ACTION_REQUEST_LOCATION = "request_location";

    public static IntentFilter getLocationUpdatedIntentFilter() {
        return new IntentFilter(LocationService.ACTION_LOCATION_UPDATED);
    }

    public static Location mLastLocation;

    public static void requestLocation(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        intent.setAction(LocationService.ACTION_REQUEST_LOCATION);
        context.startService(intent);
    }

    public LocationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        if (ACTION_REQUEST_LOCATION.equals(action)) {
            requestLocationInternal();
        } else if (ACTION_LOCATION_UPDATED.equals(action)) {
            locationUpdated(intent);
        }
    }

    /**
     * Called when a location update is requested
     */
    private void requestLocationInternal() {
        Log.v(TAG, ACTION_REQUEST_LOCATION);
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();

        // It's OK to use blockingConnect() here as we are running in an
        // IntentService that executes work on a separate (background) thread.
        ConnectionResult connectionResult = googleApiClient.blockingConnect(
                Constants.GOOGLE_API_CLIENT_TIMEOUT_S, TimeUnit.SECONDS);

        if (connectionResult.isSuccess() && googleApiClient.isConnected()) {

            Intent locationUpdatedIntent = new Intent(this, LocationService.class);
            locationUpdatedIntent.setAction(ACTION_LOCATION_UPDATED);

            // Send last known location out first if available
            Location location = FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                Intent lastLocationIntent = new Intent(locationUpdatedIntent);
                lastLocationIntent.putExtra(
                        FusedLocationProviderApi.KEY_LOCATION_CHANGED, location);
                startService(lastLocationIntent);
            }

            // Request new location
            LocationRequest mLocationRequest = new LocationRequest()
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                    .setInterval(120 * 1000);


            FusedLocationApi.requestLocationUpdates(
                    googleApiClient, mLocationRequest,
                    PendingIntent.getService(this, 0, locationUpdatedIntent, 0));
        } else {
            Log.e(TAG, String.format(Constants.GOOGLE_API_CLIENT_ERROR_MSG,
                    connectionResult.getErrorCode()));
        }
    }

    /**
     * Called when the location has been updated
     */
    private void locationUpdated(Intent intent) {
        Log.v(TAG, ACTION_LOCATION_UPDATED);

        // Extra new location
        Location location =
                intent.getParcelableExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED);

        if (location != null) {
            mLastLocation = location;
            EventBus.getDefault().post(new EventLocationChanged(
                    new LatLng(location.getLatitude(), location.getLongitude())));
            User user = Dao.user(getApplicationContext());
            DeviceRequest deviceRequest = new DeviceRequest();
            deviceRequest.setLatitude(String.valueOf(location.getLatitude()));
            deviceRequest.setLongitude(String.valueOf(location.getLongitude()));
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            deviceRequest.setImei(telephonyManager.getDeviceId());
            deviceRequest.setAppVersion(BuildConfig.VERSION_NAME);
            deviceRequest.setEventCode(user != null ? "user_logged" : "app_off");

            Intent batteryIntent = getApplicationContext().registerReceiver(null,
                    new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            int rawLevel;
            if (batteryIntent != null) {
                rawLevel = batteryIntent.getIntExtra("level", -1);
                double scale = batteryIntent.getIntExtra("scale", -1);
                double level = -1;
                if (rawLevel >= 0 && scale > 0) {
                    level = rawLevel / scale;
                }
                deviceRequest.setBattery(String.valueOf(level));
            } else {
                deviceRequest.setBattery("unknown");
            }
            deviceRequest.setSignal("unknown");
            deviceRequest.setUsername(user != null ? user.getUsername() : "no_logged_user");
            LnkApp.getLnkService().location("bearer " + (user != null ?
                    user.getToken() : ""), deviceRequest, new Callback<DeviceResponse>() {
                @Override
                public void success(DeviceResponse deviceResponse, Response response) {
                    Log.d(getClass().getName(), "Status Location: " + response.getStatus());
                }

                @Override
                public void failure(RetrofitError error) {
                    try {
                        Log.e(getClass().getName(), error.getResponse().getReason());
                    } catch (Exception ignored) {
                    }
                }
            });
        }
    }

    private static LocationService instance = null;

    public static boolean isInstanceCreated() {
        return instance != null;
    }

    public static LocationService getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        instance = null;
    }

    public static Location getLastLocation(){
        return mLastLocation;
    }
}
