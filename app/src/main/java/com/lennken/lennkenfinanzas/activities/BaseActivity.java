package com.lennken.lennkenfinanzas.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.lennken.lennkenfinanzas.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class BaseActivity extends AppCompatActivity {

    protected EventBus mBus;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(android.R.id.content)
    View mView;

    @Override
    protected void onResume() {
        super.onResume();
        mBus = EventBus.getDefault();
        mBus.register(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        setupToolbar();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBus != null) {
            mBus.unregister(this);
        }
    }

    protected void setupToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    public View getView() {
        return mView;
    }

}
