package com.lennken.lennkenfinanzas.activities;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.User;
import com.lennken.lennkenfinanzas.events.logic.EventLocationChanged;
import com.lennken.lennkenfinanzas.fragments.AboutDialogFragment;
import com.lennken.lennkenfinanzas.fragments.ModuleFragment;
import com.lennken.lennkenfinanzas.fragments.CompletedFragment;
import com.lennken.lennkenfinanzas.fragments.ChartsFragment;
import com.lennken.lennkenfinanzas.services.LocationService;
import com.lennken.lennkenfinanzas.utils.Constants;
import com.lennken.lennkenfinanzas.utils.IntentUtils;
import com.lennken.lennkenfinanzas.utils.PlayServices;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements PlayServices.OnRegisteredDevice {

    private static final String TAG = MainActivity.class.getName();
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.progress)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if(!LocationService.isInstanceCreated()) {
            LocationService.requestLocation(this);
        }
        setupToolbar();
        setupDrawer(savedInstanceState);

        PlayServices playServices = new PlayServices(getApplicationContext(),
                Constants.PREFS_NAME, Constants.SENDER_ID, this);
        playServices.register();
    }

    private void setupDrawer(Bundle savedInstanceState) {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        String drawerTitle = getResources().getString(R.string.menu_prospecting);
        if (savedInstanceState == null) {
            selectItem(drawerTitle, R.id.menu_prospecting);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onLocationChangedEvent(EventLocationChanged eventLocationChanged){
        Log.d(TAG, eventLocationChanged.latLng.toString());
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.mipmap.ic_menu_white);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        String title = menuItem.getTitle().toString();
                        selectItem(title, menuItem.getItemId());
                        return true;
                    }
                }
        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void selectItem(String title, int itemId) {
        Fragment fragment = new Fragment();
        boolean inflateFragment = true;
        switch (itemId){
            case R.id.menu_prospecting:
                fragment = ModuleFragment.newInstance("originacion");
                break;
            case R.id.menu_pre_approval:
                fragment = ModuleFragment.newInstance("preaprobacion");
                break;
            case R.id.menu_billing:
                fragment = ModuleFragment.newInstance("cobranza");
                break;
            case R.id.menu_survey:
                fragment = ModuleFragment.newInstance("encuesta");
                break;
            case R.id.menu_completed:
                fragment = new CompletedFragment();
                break;
            case R.id.menu_charts:
                fragment = new ChartsFragment();
                break;
            case R.id.menu_contact:{
                inflateFragment = false;
                User user = Dao.user(this);
                IntentUtils.callPhone(this, user.getPhone());
                break;
            }
            case R.id.menu_about:
                inflateFragment = false;
                AboutDialogFragment.newInstance().show(getSupportFragmentManager(),
                        "ABOUT_DIALOG_FRAGMENT");
                break;
            case R.id.menu_exit:
                inflateFragment = false;
                Dao.deleteUsers(this);
                IntentUtils.login(this, null);
                break;
        }
        drawerLayout.closeDrawers();
        if(inflateFragment) {
            setTitle(title);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_content, fragment)
                    .commit();
        }
    }

    @Override
    public void onRegistered(String registrationID) {
        User user = Dao.user(this);
        Realm realm = Realm.getInstance(this);
        realm.beginTransaction();
        user.setPushId(registrationID);
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
