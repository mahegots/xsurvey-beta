package com.lennken.lennkenfinanzas.activities;

/**
 * Created by Daniel García Alvarado on 8/19/15.
 * Lennken Finanzas - danielgarcia
 */

/**
 * Created by Daniel García Alvarado on 8/19/15.
 * Lennken Finanzas - danielgarcia
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.lennken.lennkenfinanzas.BuildConfig;
import com.lennken.lennkenfinanzas.LnkApp;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.User;
import com.lennken.lennkenfinanzas.utils.IntentUtils;
import com.lennken.lennkenfinanzas.utils.SnackBars;
import com.lennken.lennkenfinanzas.ws.responses.LoginResponse;
import com.lennken.lennkenfinanzas.ws.responses.UserInfoResponse;
import com.lennken.lennkenfinanzas.xsurvey.utils.Connection;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel García Alvarado on 8/19/15.
 * Lennken Finanzas - danielgarcia
 */
public class LoginActivity extends AppCompatActivity{

    public final static String TAG = LoginActivity.class.getName();

    @Bind(R.id.act_login_username)
    MaterialEditText mUsernameEditText;
    @Bind(R.id.act_login_password)
    MaterialEditText mPasswordEditText;
    @Bind(android.R.id.content)
    View mView;
    ProgressDialog mProgressDialog;
    User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if(BuildConfig.DEBUG){
            mUsernameEditText.setText("oscar");
            mPasswordEditText.setText("Passw0rd");
        }
    }

    @OnClick(R.id.act_login_signin) void onSigningClick(){
        mProgressDialog = ProgressDialog.show(this, null, getString(R.string.loading));
        mProgressDialog.setCancelable(false);
        if(Connection.isConnected(this)) {
            LnkApp.getLnkService().login("mobile", "password",
                    mUsernameEditText.getText().toString().trim(),
                    mPasswordEditText.getText().toString().trim(),
                    loginResponseCallback);
        }else{
            mProgressDialog.dismiss();
            SnackBars.withActions(this, mView, R.string.error_connection,
                    new int[]{R.string.retry}, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onSigningClick();
                        }
                    });
        }
    }

    Callback<LoginResponse> loginResponseCallback = new Callback<LoginResponse>() {
        @Override
        public void success(LoginResponse loginResponse, Response response) {
            mProgressDialog.dismiss();
            if(response.getStatus() == 200){
                mUser = new User();
                mUser.setToken(loginResponse.getAccessToken());
                mUser.setUsername(mUsernameEditText.getText().toString().trim());
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                mUser.setImei(telephonyManager.getDeviceId());
                Dao.deleteUsers(getApplicationContext());
                Dao.insert(getApplicationContext(), mUser);
                LnkApp.getLnkService()
                        .userInfo("bearer " + mUser.getToken(), mUser.getToken(), userInfoResponseCallback);
            }else if(response.getStatus() == 400){
                SnackBars.error(getApplicationContext(), mView, R.string.error_credentials);
            }else{
                Log.d(TAG, response.getReason());
                SnackBars.error(getApplicationContext(), mView, R.string.error_server);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            mProgressDialog.dismiss();
            Snackbar.make(mView, R.string.error_server, Snackbar.LENGTH_LONG).show();
        }
    };

    Callback<UserInfoResponse> userInfoResponseCallback = new Callback<UserInfoResponse>() {
        @Override
        public void success(UserInfoResponse userInfoResponse, Response response) {
            mProgressDialog.dismiss();
            if(response.getStatus() == 200){
                mUser.setPhone(userInfoResponse.getPhone());
                mUser.setClientCode(userInfoResponse.getClientCode());
                Dao.insert(getApplicationContext(), mUser);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }else if(response.getStatus() == 401){
                IntentUtils.login(LoginActivity.this, R.string.session_expired);
            }else{
                Log.d(TAG, response.getReason());
                SnackBars.error(getApplicationContext(), mView, R.string.error_server);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            mProgressDialog.dismiss();
            Log.d(TAG, error.getMessage());
            SnackBars.error(getApplicationContext(), mView, R.string.error_server);
        }
    };
}
