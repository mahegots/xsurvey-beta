package com.lennken.lennkenfinanzas;

import android.app.Application;

import com.lennken.lennkenfinanzas.ws.RetrofitEnvironments;
import com.lennken.lennkenfinanzas.ws.LnkWebService;

/**
 * Created by Daniel García Alvarado on 8/19/15.
 * Lennken Finanzas - danielgarcia
 */
public class LnkApp extends Application{

    static LnkWebService lnkService;
    static LnkWebService lnkMockService;

    @Override
    public void onCreate() {
        super.onCreate();
        lnkService = RetrofitEnvironments.createQAEnvironment();
        if(BuildConfig.DEBUG){
            lnkMockService = RetrofitEnvironments.createMockEnvironment(getApplicationContext());
        }
    }


    public static LnkWebService getLnkService() {
        return lnkService;
    }

    public static LnkWebService getLnkMockService(){
        return lnkMockService;
    }
}
