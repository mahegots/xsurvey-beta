package com.lennken.lennkenfinanzas.xsurvey.events.ui;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventPreviewImageClick {
    public Question question;

    public EventPreviewImageClick(Question question) {
        this.question = question;
    }
}
