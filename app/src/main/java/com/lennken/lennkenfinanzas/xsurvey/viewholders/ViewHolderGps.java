package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Response;
import com.lennken.lennkenfinanzas.services.LocationService;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;

import de.greenrobot.event.EventBus;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderGps extends RecyclerView.ViewHolder {

    public ViewHolderGps(View v) {
        super(v);
    }


    public void location(Question question) {
        if (LocationService.getLastLocation() != null) {
            if (question.getResponseArray().size() == 0) {
                Response latitude = new Response();
                latitude.setIdResponse(-1);
                latitude.setResponse(String.valueOf(LocationService.getLastLocation().getLatitude()));
                Response longitude = new Response();
                longitude.setIdResponse(-1);
                longitude.setResponse(String.valueOf(LocationService.getLastLocation().getLongitude()));
                EventBus.getDefault().postSticky(new EventQuestionResponse(question, latitude));
                EventBus.getDefault().postSticky(new EventQuestionResponse(question, longitude));

            }
        }
    }
}