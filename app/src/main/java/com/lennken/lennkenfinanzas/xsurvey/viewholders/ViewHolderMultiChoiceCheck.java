package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Response;
import com.lennken.lennkenfinanzas.xsurvey.enums.AnswerType;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.utils.Utils;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;

import de.greenrobot.event.EventBus;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderMultiChoiceCheck extends RecyclerView.ViewHolder {
    public TextView textView;
    public LinearLayout linearLayout;
    public MaterialEditText editText;
    public HashMap<String, CheckedChangeImpl> checkedChangeListenerHashMap;

    public ViewHolderMultiChoiceCheck(View v) {
        super(v);
        linearLayout = (LinearLayout) v.findViewById(R.id.item_survey_list_check_container);
        textView = (TextView) v.findViewById(R.id.item_survey_list_list_title);
        editText = (MaterialEditText)v.findViewById(R.id.item_survey_list_edit_text);
        checkedChangeListenerHashMap = new HashMap<>();
    }

    public CompoundButton.OnCheckedChangeListener getCheckedChangeListener(CheckBox checkBox,
                                                                           Question question) {
        Answer answer = (Answer) checkBox.getTag();
        if(!checkedChangeListenerHashMap.containsKey(answer.getUuid())){
            checkedChangeListenerHashMap.put(answer.getUuid(), new CheckedChangeImpl(this, question));
        }
        return checkedChangeListenerHashMap.get(answer.getUuid());
    }

    public class CheckedChangeImpl implements CompoundButton.OnCheckedChangeListener{

        private final ViewHolderMultiChoiceCheck vh;
        private final Question question;

        public CheckedChangeImpl(ViewHolderMultiChoiceCheck vh, Question question){
            this.vh = vh;
            this.question = question;
        }
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            Answer answerTag = (Answer) compoundButton.getTag();
            vh.editText.setVisibility(b && answerTag.getType() == AnswerType.OTHER
                    ? View.VISIBLE : View.GONE);
            if (b) {
                Response response = new Response();
                response.setResponse(answerTag.getTitle());
                response.setIdResponse(answerTag.getAnswerId());
                EventBus.getDefault().post(new EventQuestionResponse(question, response));
            }else{
                Response response = Utils.responseByIdInArray(
                        answerTag.getAnswerId(), question.getResponseArray());
                if(response != null){
                    EventBus.getDefault().post(new EventQuestionResponse(question, response, true));
                }
            }
        }
    }
}