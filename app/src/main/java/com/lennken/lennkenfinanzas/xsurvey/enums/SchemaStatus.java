package com.lennken.lennkenfinanzas.xsurvey.enums;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class SchemaStatus {
    public static final int INCOMPLETE = 1;
    public static final int COMPLETE = 2;
    public static final int PENDING_SYNC = 3;
    public static final int SYNCED = 4;
    public static final int CANCELLED = 5;
}
