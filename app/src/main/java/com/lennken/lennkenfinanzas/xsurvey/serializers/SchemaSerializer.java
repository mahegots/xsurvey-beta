package com.lennken.lennkenfinanzas.xsurvey.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lennken.lennkenfinanzas.db.models.Schema;

import java.lang.reflect.Type;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class SchemaSerializer implements JsonSerializer<Schema>{
    @Override
    public JsonElement serialize(Schema src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("uuid",src.getUuid());
        jsonObject.addProperty("idModule", src.getIdModule());
        jsonObject.addProperty("idVisit", src.getIdVisit());
        jsonObject.addProperty("idWorkPlan", src.getIdWorkPlan());
        jsonObject.addProperty("mobileType", src.getMobileType());
        jsonObject.addProperty("pushId", src.getPushId());
        jsonObject.addProperty("schemaVersion", src.getSchemaVersion());
        jsonObject.addProperty("status", src.getStatus());
        jsonObject.addProperty("username", src.getUsername());
        jsonObject.addProperty("latitude", src.getLatitude());
        jsonObject.addProperty("longitude", src.getLongitude());
        jsonObject.add("surveys", context.serialize(src.getSurveys()));
        return jsonObject;
    }
}
