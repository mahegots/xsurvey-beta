package com.lennken.lennkenfinanzas.xsurvey.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.SignatureEvent;

import java.io.ByteArrayOutputStream;

import de.greenrobot.event.EventBus;

/**
 * Created by Daniel García Alvarado on 3/1/15.
 * Lennken Finanzas - angelgomez
 */
@SuppressLint("ValidFragment")
public class SignatureDialogFragment extends DialogFragment {

    private final Question question;
    private int mPosition;

    private LinearLayout mContent;
    private Bitmap mSignatureBitmap;
    private Signature mSignature;

    public SignatureDialogFragment(Question question) {
        this.question = question;
    }

    public static SignatureDialogFragment newInstance(Question question) {
        return new SignatureDialogFragment(question);
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_signature_dialog, null);

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics mMetrics = new DisplayMetrics();
        display.getMetrics(mMetrics);
        int mWidthFull = mMetrics.widthPixels;
        int mWidth = (int) Math.round(mWidthFull * .9);
        int mHeight = (int) Math.round(mWidth * .75);

        mContent = (LinearLayout) view.findViewById(R.id.draw_signature);
        mContent.setLayoutParams(new LinearLayout.LayoutParams(mWidth, mHeight));

        mSignature = new Signature(getActivity(), null);

        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, mWidth, mHeight);

        mBuilder.setView(view)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Bitmap bitmap = mSignature.save(mContent);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 65, stream);
                        byte[] byteArray = stream.toByteArray();
                        EventBus.getDefault()
                                .post(new EventQuestionResponse(question,
                                        Base64.encodeToString(byteArray, Base64.DEFAULT)));
                        EventBus.getDefault().post(new SignatureEvent(question));
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SignatureDialogFragment.this.getDialog().cancel();
                    }
                });

        return mBuilder.create();
    }

    public class Signature extends View
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public Signature (Context context, AttributeSet attrs)
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public Bitmap save (View v) {
            if(mSignatureBitmap == null) {
                mSignatureBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(),
                        Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(mSignatureBitmap);
            v.draw(canvas);

            return mSignatureBitmap;
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(@NonNull MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++)
                    {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left)
            {
                dirtyRect.left = historicalX;
            }
            else if (historicalX > dirtyRect.right)
            {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top)
            {
                dirtyRect.top = historicalY;
            }
            else if (historicalY > dirtyRect.bottom)
            {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}
