package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventOpenTable;

import de.greenrobot.event.EventBus;


/**
 * Created by Daniel García Alvarado on 8/27/15.
 * LennkenFinanzas - danielgarcia
 */
public class ViewHolderTable extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textView;
    public Button button;

    public ViewHolderTable(View itemView) {
        super(itemView);
        textView = (TextView)itemView.findViewById(R.id.item_survey_table_title);
        button = (Button)itemView.findViewById(R.id.item_survey_table_open);
        itemView.findViewById(R.id.item_survey_table_open).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Question question = (Question) view.getTag();
        EventBus.getDefault().post(new EventOpenTable(question));
    }
}
