package com.lennken.lennkenfinanzas.xsurvey.enums;

/**
 * Created by angelgomez on 7/21/15.
 */
public class QuestionType {

    public static final int DATE = 1;
    public static final int MULTI_CHOICE_SPINNER = 2;
    public static final int MULTI_CHOICE_RADIO = 3;
    public static final int MULTI_CHOICE_CHECK = 4;
    public static final int SECTION_LABEL = 5;
    public static final int DISCLAIMER = 6;
    public static final int IMAGE = 7;
    public static final int SIGNATURE = 8;
    public static final int OPEN_ENDED = 9;
    public static final int TIME = 10;
    public static final int START_DATE = 12;
    public static final int END_DATE = 13;
    public static final int GPS = 14;
    public static final int MAP = 15;
    public static final int TABLE_QUESTION = 16;
    public static final int LATITUDE = 17;
    public static final int LONGITUDE = 18;
    public static final int BARCODESCANNER = 19;

}
