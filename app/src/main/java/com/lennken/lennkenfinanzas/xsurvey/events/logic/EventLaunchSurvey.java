package com.lennken.lennkenfinanzas.xsurvey.events.logic;

import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.Survey;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventLaunchSurvey {
    public Survey survey;
    public Schema schema;

    public EventLaunchSurvey(Survey survey, Schema schema) {
        this.survey = survey;
        this.schema = schema;
    }
}
