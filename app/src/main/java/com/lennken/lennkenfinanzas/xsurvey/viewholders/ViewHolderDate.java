package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.rengwuxian.materialedittext.MaterialEditText;

import de.greenrobot.event.EventBus;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderDate extends RecyclerView.ViewHolder {
    public MaterialEditText editText;

    public ViewHolderDate(View v) {
        super(v);
        editText = (MaterialEditText) v.findViewById(R.id.item_survey_list_edit_text);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                                          int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Question question = (Question) editText.getTag();
                EventBus.getDefault().post(new EventQuestionResponse(question, editable.toString()));
            }
        });
    }
}