package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventCoordinateCaptured;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventImageCaptured;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventCaptureCoordinateClick;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventPreviewCoordinateClick;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventPreviewImageClick;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

/**
 * Created by Daniel García Alvarado on 8/25/15.
 * LennkenFinanzas - danielgarcia
 */
public class ViewHolderMap extends RecyclerView.ViewHolder implements View.OnClickListener{
    public LinearLayout linearLayout;
    public TextView textView;

    public ViewHolderMap(View v) {
        super(v);
        v.findViewById(R.id.item_survey_map_capture).setOnClickListener(this);
        v.findViewById(R.id.item_survey_map_preview).setOnClickListener(this);
        v.findViewById(R.id.item_survey_map_clean).setOnClickListener(this);
        linearLayout = (LinearLayout) v.findViewById(R.id.item_survey_map_container);
        textView = (TextView) v.findViewById(R.id.item_survey_map_title);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onClick(View view) {
        Question question = (Question) textView.getTag();
        switch (view.getId()){
            case R.id.item_survey_map_capture:
                EventBus.getDefault().post(new EventCaptureCoordinateClick(question));
                break;
            case R.id.item_survey_map_preview:
                EventBus.getDefault().post(new EventPreviewCoordinateClick(question));
                break;
            case R.id.item_survey_map_clean:
                EventBus.getDefault().post(new EventQuestionResponse(question, ""));
                linearLayout.setVisibility(View.GONE);
                break;
        }
    }

    @Subscribe
    public void onEventCapturedCoordinate(EventCoordinateCaptured event){
        Question question = (Question) textView.getTag();
        if(event.question.getIdQuestion() == question.getIdQuestion()){
            linearLayout.setVisibility(View.VISIBLE);
        }
    }
}