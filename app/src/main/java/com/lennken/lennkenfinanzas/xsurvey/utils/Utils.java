package com.lennken.lennkenfinanzas.xsurvey.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Response;
import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.Survey;
import com.lennken.lennkenfinanzas.xsurvey.serializers.QuestionSerializer;
import com.lennken.lennkenfinanzas.xsurvey.serializers.ResponseSerializer;
import com.lennken.lennkenfinanzas.xsurvey.serializers.SchemaSerializer;
import com.lennken.lennkenfinanzas.xsurvey.serializers.SurveySerializer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class Utils {

    public static int getToolbarHeight(Context context) {
        return (int) context.getResources().getDimension(R.dimen.abc_action_bar_default_height_material);
    }

    public static int getStatusBarHeight(Context context) {
        return (int) context.getResources().getDimension(R.dimen.status_bar_size);
    }

    public static int answerPositionByIdInArray(int idResponse, RealmList<Answer> answers) {
        if (answers == null)
            return 0;
        int i = 0;
        boolean found = false;
        for (Answer answer : answers) {
            if (answer.getAnswerId() == idResponse) {
                found = true;
                break;
            }
            i++;
        }
        return found ? i : 0;
    }

    @Nullable
    public static Response responseByIdInArray(int answerId, RealmList<Response> responseArray) {
        for (Response response : responseArray) {
            if (response.getIdResponse() == answerId)
                return response;
        }
        return null;
    }

    public static Gson gsonForSerialization() throws ClassNotFoundException {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                });
        gsonBuilder.registerTypeAdapter(Schema.class, new SchemaSerializer())
                .registerTypeAdapter(Survey.class, new SurveySerializer())
                .registerTypeAdapter(Response.class, new ResponseSerializer())
                .registerTypeAdapter(Question.class, new QuestionSerializer());

        gsonBuilder.registerTypeAdapter(Class.forName("io.realm.SchemaRealmProxy"), new SchemaSerializer())
                .registerTypeAdapter(Class.forName("io.realm.SurveyRealmProxy"), new SurveySerializer())
                .registerTypeAdapter(Class.forName("io.realm.ResponseRealmProxy"), new ResponseSerializer())
                .registerTypeAdapter(Class.forName("io.realm.QuestionRealmProxy"), new QuestionSerializer());
        return gsonBuilder.create();
    }

    @SuppressLint("SimpleDateFormat")
    public static String getDayDateString(String dateStr) {
        DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String formatted = null;
        try {
            Date date = formatDate.parse(dateStr);
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            format.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
            formatted = format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatted;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
