package com.lennken.lennkenfinanzas.xsurvey.activities;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.services.LocationService;
import com.lennken.lennkenfinanzas.xsurvey.events.CoordinateEvent;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventCoordinateCaptured;

import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

public class MapsActivity extends FragmentActivity{
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Question mQuestion;
    private Location mLocation;
    private Marker mQuestionMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        setUpMapIfNeeded();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        mMap.clear();
                        mQuestionMarker = mMap.addMarker(new MarkerOptions().position(latLng)
                                .title(mQuestion.getQuestion())
                                .draggable(false));
                    }
                });
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mLocation = LocationService.getLastLocation();
        if (mLocation != null) {
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(CameraPosition
                                    .fromLatLngZoom(
                                            new LatLng(mLocation.getLatitude(),
                                                    mLocation.getLongitude()),
                                            14)
                    ));
        }
    }

    @Subscribe(sticky = true)
    public void onLaunch(CoordinateEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        mQuestion = event.question;
        if (mMap != null) {
            if (!mQuestion.getResponse().isEmpty()) {
                String[] location = mQuestion.getResponse().split(",");
                mMap.stopAnimation();
                LatLng latLng = new LatLng(Float.valueOf(location[0]), Float.valueOf(location[1]));
                mQuestionMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(mQuestion.getQuestion()).draggable(false));
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(CameraPosition.fromLatLngZoom(latLng, 14)));
            } else {
                if (mLocation != null) {
                    mQuestionMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(mLocation.getLatitude(),
                            mLocation.getLongitude()))
                            .title(mQuestion.getQuestion())
                            .draggable(false));
                } else {
                    mQuestionMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0))
                            .title(mQuestion.getQuestion())
                            .draggable(false));
                }
            }
        }
    }

    @OnClick(R.id.act_map_save)
    void onSaveClick() {
        LatLng latLng = mQuestionMarker.getPosition();
        EventBus.getDefault().post(new EventQuestionResponse(mQuestion,
                latLng.latitude + "," + latLng.longitude));
        EventBus.getDefault().post(new EventCoordinateCaptured(mQuestion));
        finish();
    }
}
