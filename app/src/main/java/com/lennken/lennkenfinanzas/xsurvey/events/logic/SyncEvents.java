package com.lennken.lennkenfinanzas.xsurvey.events.logic;

import retrofit.RetrofitError;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class SyncEvents {

    public static class SyncStartEvent{}
    public static class SyncFinishEvent {}

    public static class SyncErrorEvent {
        public Exception error;

        public SyncErrorEvent(Exception error) {
            this.error = error;
        }
    }
}
