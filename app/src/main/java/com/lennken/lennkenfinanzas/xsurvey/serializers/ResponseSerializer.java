package com.lennken.lennkenfinanzas.xsurvey.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lennken.lennkenfinanzas.db.models.Response;

import java.lang.reflect.Type;

/**
 * Created by Daniel García Alvarado on 8/31/15.
 * LennkenFinanzas - danielgarcia
 */
public class ResponseSerializer implements JsonSerializer<Response>{
    @Override
    public JsonElement serialize(Response src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("response", src.getResponse());
        jsonObject.addProperty("idResponse", src.getIdResponse());
        return jsonObject;
    }
}
