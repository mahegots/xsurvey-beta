package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;

import de.greenrobot.event.EventBus;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderMultiChoiceSpinner extends RecyclerView.ViewHolder {
    public MaterialSpinner spinner;

    public ViewHolderMultiChoiceSpinner(final View v) {
        super(v);
        spinner = (MaterialSpinner) v.findViewById(R.id.item_survey_list_spinner);
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        if (AdapterView.INVALID_POSITION != position) {
                            Question questionTag = (Question) adapterView.getTag();
                            EventQuestionResponse event = new EventQuestionResponse(questionTag,
                                    questionTag.getAnswers().get(position).getTitle());
                            event.position = position;
                            event.idResponse = questionTag.getAnswers().get(position).getAnswerId();
                            EventBus.getDefault().post(event);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });
    }
}
