package com.lennken.lennkenfinanzas.xsurvey.events.ui;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by MobileDev on 28/08/15.
 */
public class EventOpenTable {
    public Question question;

    public EventOpenTable(Question question) {
        this.question = question;
    }
}
