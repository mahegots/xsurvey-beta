package com.lennken.lennkenfinanzas.xsurvey.events;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by Daniel García Alvarado on 8/25/15.
 * LennkenFinanzas - danielgarcia
 */
public class CoordinateEvent {
    public Question question;

    public CoordinateEvent(Question question){
        this.question = question;
    }
}
