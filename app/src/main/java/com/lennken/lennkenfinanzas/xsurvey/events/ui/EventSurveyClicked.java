package com.lennken.lennkenfinanzas.xsurvey.events.ui;

import com.lennken.lennkenfinanzas.db.models.Survey;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventSurveyClicked {
    public Survey survey;

    public EventSurveyClicked(Survey survey) {
        this.survey = survey;
    }
}
