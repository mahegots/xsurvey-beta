package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */

public class ViewHolderDisclaimer extends RecyclerView.ViewHolder {
    public TextView textView;

    public ViewHolderDisclaimer(View v) {
        super(v);
        textView = (TextView)
                v.findViewById(R.id.item_survey_list_disclaimer_text_view);
    }
}