package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderSectionLabel extends RecyclerView.ViewHolder {
    public TextView textView;

    public ViewHolderSectionLabel(View v) {
        super(v);
        textView = (TextView)
                v.findViewById(R.id.item_survey_list_section_title_text_view);
    }
}