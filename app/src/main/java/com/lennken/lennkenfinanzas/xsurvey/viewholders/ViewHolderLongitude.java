package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.services.LocationService;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;

import de.greenrobot.event.EventBus;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderLongitude extends RecyclerView.ViewHolder {

    public ViewHolderLongitude(View v) {
        super(v);
    }


    public void location(Question question) {
        if (LocationService.getLastLocation() != null) {
            if (question.getResponseArray().size() == 0) {
                double longitude = LocationService.getLastLocation().getLongitude();
                EventBus.getDefault().post(new EventQuestionResponse(question, String.valueOf(longitude)));
            }
        }
    }
}