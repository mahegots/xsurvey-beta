package com.lennken.lennkenfinanzas.xsurvey.events.logic;

import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Question;

import io.realm.RealmList;

/**
 * Created by MobileDev on 28/08/15.
 */
public class EventAnswer {

    public Question question;
    public  RealmList<Answer> answers;

    public EventAnswer(Question question, RealmList<Answer> answers) {
        this.question = question;
        this.answers = answers;
    }
}
