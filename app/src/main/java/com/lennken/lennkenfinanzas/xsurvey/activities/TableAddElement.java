package com.lennken.lennkenfinanzas.xsurvey.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.activities.BaseActivity;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Response;
import com.lennken.lennkenfinanzas.xsurvey.adapters.AnswerAdapter;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventAnswer;
import com.lennken.lennkenfinanzas.xsurvey.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.Subscribe;
import io.realm.RealmList;

/**
 * Created by MobileDev on 28/08/15.
 */
public class TableAddElement extends BaseActivity {


    @Bind(R.id.act_survey_table_recycler)
    RecyclerView recyclerView;
    AnswerAdapter mTableElementAdapter;
    ArrayList<String> answerArrayList;
    private Question mQuestion;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_add_item);
        ButterKnife.bind(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mTableElementAdapter);
        answerArrayList = new ArrayList<>();
        answerArrayList.add(0, "");
    }

    @Subscribe(sticky = true)
    public void onEvent(EventAnswer eventAnswer) {
        mQuestion = eventAnswer.question;
        mTableElementAdapter = new AnswerAdapter(this, getSupportFragmentManager(),
                eventAnswer.answers, new HashMap<String, Response>());
        recyclerView.setAdapter(mTableElementAdapter);
    }



    @OnClick(R.id.act_survey_table_save)
    public void onClick() {
        HashMap<String, Response> responseHashMap = mTableElementAdapter.getResponseHashMap();
        RealmList<Response> responses = new RealmList<>();
        for(Map.Entry<String, Response> entry : responseHashMap.entrySet()){
            responses.add(entry.getValue());
        }
        try {
            Response response = new Response(Utils.gsonForSerialization().toJson(responses), -1);
            mBus.postSticky(new EventQuestionResponse(mQuestion, response));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        /*mBus.post(new EventTableChanges());
        for (String obj : answerArrayList) {
            Log.e("Data", obj);
        }*/
        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.incomplete_survey)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }
}
