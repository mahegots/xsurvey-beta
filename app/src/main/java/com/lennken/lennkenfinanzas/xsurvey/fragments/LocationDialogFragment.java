package com.lennken.lennkenfinanzas.xsurvey.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.services.LocationService;
import com.lennken.lennkenfinanzas.utils.MapsUtils;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.LocationEvent;

import de.greenrobot.event.EventBus;

/**
 * Created by angelgomez on 8/6/15.
 */
public class LocationDialogFragment extends DialogFragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private String mLocationString = "";

    private Location mLastLocation;

    private GoogleApiClient mGoogleApiClient;

    private TextView mLocationTextView;

    public static final String BUNDLE_POSITION = "bundle_position";

    public static final String TAG = "location_dialog_fragment";

    public static LocationDialogFragment newInstance(int position) {
        LocationDialogFragment mDialogFragment = new LocationDialogFragment();

        Bundle mBundle = new Bundle();
        mBundle.putInt(BUNDLE_POSITION, position);
        mDialogFragment.setArguments(mBundle);

        return mDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_location_dialog, null);

        mLocationTextView = (TextView) view.findViewById(R.id.fragment_location_dialog_text_view);

        Bundle mBundle = getArguments();
        final int mPostion = mBundle.getInt(BUNDLE_POSITION);

        mBuilder.setView(view)
                .setTitle(R.string.location)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MapsUtils maps= new MapsUtils(mLocationString);
                        maps.getLat();
                        maps.getLon();
                        EventBus.getDefault().post(new LocationEvent(mLocationString, mPostion));
                    }
                })
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        return mBuilder.create();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            mLocationString = String.valueOf(mLastLocation.getLatitude()) + ", " +
                    String.valueOf(mLastLocation.getLongitude());
            mLocationTextView.setText(mLocationString);
        } else {
            mLocationTextView.setText(getResources().getString(R.string.no_location_detected));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        LocationService.getLastLocation().getLongitude();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mGoogleApiClient.connect();
    }

   }
