package com.lennken.lennkenfinanzas.xsurvey.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lennken.lennkenfinanzas.db.models.Question;

import java.lang.reflect.Type;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class QuestionSerializer implements JsonSerializer<Question>{
    @Override
    public JsonElement serialize(Question src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("idQuestion", src.getIdQuestion());
        jsonObject.addProperty("idResponse", src.getIdResponse());
        jsonObject.addProperty("position", src.getPosition());
        jsonObject.addProperty("question", src.getQuestion());
        jsonObject.addProperty("questionType", src.getQuestionType());
        jsonObject.addProperty("response", src.getResponse());
        return jsonObject;
    }
}
