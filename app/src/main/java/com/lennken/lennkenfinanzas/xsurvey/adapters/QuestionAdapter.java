package com.lennken.lennkenfinanzas.xsurvey.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Response;
import com.lennken.lennkenfinanzas.xsurvey.enums.AnswerType;
import com.lennken.lennkenfinanzas.xsurvey.enums.QuestionType;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.utils.Adapters;
import com.lennken.lennkenfinanzas.xsurvey.utils.Utils;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderBarcodeScanner;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderDate;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderDisclaimer;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderEmpty;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderGps;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderImage;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderLatitude;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderLongitude;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderMap;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderMultiChoiceCheck;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderMultiChoiceSpinner;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderOpenEnded;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderRadioGroup;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderSectionLabel;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderSignature;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderTable;

import java.util.Calendar;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class QuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = "question_adapter";
    private final Activity context;
    private final FragmentManager fm;
    int i = 0;
    private RealmList<Question> questions;

    public QuestionAdapter(Activity context, FragmentManager fm, RealmList<Question> questions) {
        this.questions = questions;
        this.context = context;
        this.fm = fm;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        switch (viewType) {
            case (QuestionType.OPEN_ENDED): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_open_ended, parent, false);
                return new ViewHolderOpenEnded(v);
            }
            case (QuestionType.MULTI_CHOICE_SPINNER): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_multi_choice_spinner, parent, false);
                return new ViewHolderMultiChoiceSpinner(v);
            }
            case (QuestionType.MULTI_CHOICE_RADIO): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_multi_choice_radio, parent, false);
                return new ViewHolderRadioGroup(v);
            }
            case (QuestionType.MULTI_CHOICE_CHECK): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_multi_choice_check, parent, false);
                return new ViewHolderMultiChoiceCheck(v);
            }
            case (QuestionType.SECTION_LABEL): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_section_label, parent, false);
                return new ViewHolderSectionLabel(v);
            }
            case (QuestionType.DISCLAIMER): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_disclaimer, parent, false);
                return new ViewHolderDisclaimer(v);
            }
            case (QuestionType.IMAGE): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_image, parent, false);
                return new ViewHolderImage(v);
            }
            case (QuestionType.SIGNATURE): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_image, parent, false);
                return new ViewHolderSignature(v, fm);
            }
            case (QuestionType.DATE): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_date, parent, false);
                return new ViewHolderDate(v);
            }
            case (QuestionType.GPS): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.divider, parent, false);
                return new ViewHolderGps(v);
            }
            case (QuestionType.MAP): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_map, parent, false);
                return new ViewHolderMap(v);
            }
            case (QuestionType.TABLE_QUESTION): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_table, parent, false);
                return new ViewHolderTable(v);
            }
            case (QuestionType.LATITUDE): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.divider, parent, false);
                return new ViewHolderLatitude(v);
            }
            case (QuestionType.LONGITUDE): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.divider, parent, false);
                return new ViewHolderLongitude(v);
            }
            case (QuestionType.BARCODESCANNER): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_barcode, parent, false);
                return new ViewHolderBarcodeScanner(v,fm);
            }
            default:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list, parent, false);
                return new ViewHolderEmpty(v);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder h, final int position) {
        final Question question = questions.get(position);
        switch (h.getItemViewType()) {
            //region OPEN_ENDED
            case (QuestionType.OPEN_ENDED): {
                final ViewHolderOpenEnded holderOpenEnded = (ViewHolderOpenEnded) h;
                holderOpenEnded.editText.setTag(question);
                holderOpenEnded.editText.setVisibility(View.VISIBLE);
                holderOpenEnded.editText.setHint(question.getQuestion());
                holderOpenEnded.editText.setFloatingLabelText(question.getQuestion());
                holderOpenEnded.editText.setText(question.getResponse());
                break;
            }
            //endregion

            //region MULTI_CHOICE_SPINNER
            case (QuestionType.MULTI_CHOICE_SPINNER): {
                ViewHolderMultiChoiceSpinner holderMultiChoiceSpinner = (ViewHolderMultiChoiceSpinner) h;
                holderMultiChoiceSpinner.spinner.setTag(question);
                holderMultiChoiceSpinner.spinner.setAdapter(Adapters.getAdapter(question));
                holderMultiChoiceSpinner.spinner.setHint(question.getQuestion());
                holderMultiChoiceSpinner.spinner.setBaseColor(context.getResources()
                        .getColor(R.color.primary_dark));
                Log.d(getClass().getName(), "Position precharged: " + position);
                holderMultiChoiceSpinner.spinner.setSelection(question.getArrayPosition() + 1, true);
                break;
            }
            //endregion

            //region MULTI_CHOICE_RADIO
            case (QuestionType.MULTI_CHOICE_RADIO): {
                Log.d(TAG, String.valueOf(getItemViewType(position)));
                final ViewHolderRadioGroup vh = (ViewHolderRadioGroup) h;
                vh.textView.setText(question.getQuestion());
                vh.radioGroup.removeAllViews();
                vh.editText.setText("");
                for (int i = 0; i < question.getAnswers().size(); i++) {
                    final Answer answer = question.getAnswers().get(i);
                    final RadioButton radioButton = new RadioButton(context);
                    radioButton.setTag(answer);
                    radioButton.setText(answer.getTitle());
                    radioButton.setId(View.generateViewId());
                    radioButton.setOnCheckedChangeListener(
                            vh.getCheckedChangeListener(radioButton, question, i)
                    );
                    vh.radioGroup.addView(radioButton);
                }
                Log.d(getClass().getName(), "Position precharded: " + question.getArrayPosition());
                RadioButton selectedRadio = (RadioButton) vh.radioGroup
                        .getChildAt(question.getArrayPosition());
                vh.radioGroup.check(selectedRadio.getId());
                vh.editText.setText(question.getResponse());
                break;
            }
            //endregion

            //region MULTI_CHOICE_CHECK
            case (QuestionType.MULTI_CHOICE_CHECK): {
                Log.d(TAG, String.valueOf(getItemViewType(position)));
                final ViewHolderMultiChoiceCheck vh = (ViewHolderMultiChoiceCheck) h;
                vh.textView.setText(question.getQuestion());
                vh.linearLayout.removeAllViews();
                vh.editText.setText("");
                vh.editText.setVisibility(View.GONE);
                for (int i = 0; i < question.getAnswers().size(); i++) {
                    final Answer answer = question.getAnswers().get(i);
                    final CheckBox checkBox = new CheckBox(context);
                    checkBox.setTag(answer);
                    checkBox.setText(answer.getTitle());
                    checkBox.setId(View.generateViewId());
                    checkBox.setOnCheckedChangeListener(
                            vh.getCheckedChangeListener(checkBox, question)
                    );
                    vh.linearLayout.addView(checkBox);
                }
                RealmList<Answer> answers = question.getAnswers();
                for (Response response : question.getResponseArray()) {
                    int checkPosition = Utils.answerPositionByIdInArray(
                            question.getIdResponse(), answers);
                    CheckBox checkBox = (CheckBox) vh.linearLayout.getChildAt(checkPosition);
                    checkBox.setChecked(true);
                    if (response.getIdResponse() == AnswerType.OTHER)
                        vh.editText.setText(response.getResponse());
                }
                break;
            }
            //endregion

            //region SECTION_LABEL
            case (QuestionType.SECTION_LABEL): {
                ViewHolderSectionLabel viewHolderSectionLabel = (ViewHolderSectionLabel) h;
                viewHolderSectionLabel.textView.setText(question.getQuestion());
                //viewHolderSectionLabel.textView.setPadding(0, 0, 0, 2);
                break;
            }
            //endregion

            //region DISCLAIMER
            case (QuestionType.DISCLAIMER): {
                ViewHolderDisclaimer viewHolderDisclaimer = (ViewHolderDisclaimer) h;
                viewHolderDisclaimer.textView.setText(question.getQuestion());
                break;
            }
            //endregion

            //region IMAGE
            case (QuestionType.IMAGE): {
                ViewHolderImage viewHolderImage = (ViewHolderImage) h;
                viewHolderImage.textView.setText(question.getQuestion());
                viewHolderImage.textView.setTag(question);
                if (!question.getResponse().isEmpty()) {
                    viewHolderImage.linearLayout.setVisibility(View.VISIBLE);
                }
                break;
            }
            //endregion

            //region IMAGE
            case (QuestionType.MAP): {
                ViewHolderMap viewHolderImage = (ViewHolderMap) h;
                viewHolderImage.textView.setText(question.getQuestion());
                viewHolderImage.textView.setTag(question);
                if (!question.getResponse().isEmpty()) {
                    viewHolderImage.linearLayout.setVisibility(View.VISIBLE);
                }
                break;
            }
            //endregion

            //region SIGNATURE
            case (QuestionType.SIGNATURE): {
                ViewHolderSignature viewHolderSignature = (ViewHolderSignature) h;
                viewHolderSignature.textView.setText(question.getQuestion());
                viewHolderSignature.textView.setTag(question);
                if (!question.getResponse().isEmpty()) {
                    viewHolderSignature.linearLayout.setVisibility(View.VISIBLE);
                }
                break;
            }
            //endregion

            //region DATE
            case (QuestionType.DATE): {
                final ViewHolderDate viewHolderDate = (ViewHolderDate) h;
                viewHolderDate.editText.setTag(question);
                viewHolderDate.editText.setVisibility(View.VISIBLE);
                viewHolderDate.editText.setFocusable(false);
                viewHolderDate.editText.setHint(question.getQuestion());
                viewHolderDate.editText.setFloatingLabelText(question.getQuestion());
                Calendar mCalendar = Calendar.getInstance();

                final DatePickerDialog mDatePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(
                                    DatePickerDialog datePickerDialog, int year, int month,
                                    int day) {
                                String mDate = String.valueOf(year) + "/" + String.valueOf(month + 1)
                                        + "/" + String.valueOf(day);
                                viewHolderDate.editText.setText(mDate);
                            }
                        }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH), true);
                mDatePickerDialog.setCancelable(false);
                viewHolderDate.editText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDatePickerDialog.show(fm, "date_picker");
                    }
                });
                viewHolderDate.editText.setText(question.getResponse());
                break;
            }
            //endregion

            //region GPS
            case (QuestionType.GPS): {
                ViewHolderGps viewHolderGps = (ViewHolderGps) h;
                viewHolderGps.location(question);
                break;
            }
            //region GPS no Visible
            case (QuestionType.TABLE_QUESTION): {
                ViewHolderTable viewHolderTable = (ViewHolderTable) h;
                viewHolderTable.textView.setText(question.getQuestion());
                viewHolderTable.button.setTag(question);
                break;
            }
            //endregion
            case (QuestionType.LATITUDE): {
                ViewHolderLatitude viewHolderLatitude = (ViewHolderLatitude) h;
                viewHolderLatitude.location(question);
                break;
            }
            //endregion
            case (QuestionType.LONGITUDE): {
                ViewHolderLongitude viewHolderLongitude = (ViewHolderLongitude) h;
                viewHolderLongitude.location(question);
                break;
            }
            //endregion
            case (QuestionType.BARCODESCANNER): {
                ViewHolderBarcodeScanner viewHolderBarcodeScanner = (ViewHolderBarcodeScanner) h;
                viewHolderBarcodeScanner.textView.setText(question.getQuestion());
                viewHolderBarcodeScanner.textView.setTag(question);
                if(question.getResponse() != null && !question.getResponse().isEmpty()){
                    viewHolderBarcodeScanner.ean2EditText.setText(question.getResponse());
                }
                break;
            }
            //endregion
            case (QuestionType.START_DATE):
                break;
            case (QuestionType.END_DATE):
                break;

        }
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    @Override
    public int getItemViewType(int position) {
        return questions.get(position).getQuestionType();
    }

    public void setQuestionsList(RealmList<Question> questionsList) {
        this.questions = questionsList;
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventQuestionResponse(EventQuestionResponse event) {
        EventBus.getDefault().removeAllStickyEvents();
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        event.question.setIdResponse(event.idResponse);
        event.question.setResponse(event.response);
        event.question.setArrayPosition(event.position);
        if (event.responseArray != null) {
            if (!event.remove) {
                event.responseArray = realm.copyToRealmOrUpdate(event.responseArray);
                event.question.getResponseArray().add(event.responseArray);
            } else {
                event.question.getResponseArray().remove(event.responseArray);
            }
        }
        realm.commitTransaction();
    }

    public void unregister() {
        EventBus.getDefault().unregister(this);
    }

    public void register() {
        EventBus.getDefault().register(this);
    }
}