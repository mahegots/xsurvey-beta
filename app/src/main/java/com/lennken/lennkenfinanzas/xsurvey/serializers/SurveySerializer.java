package com.lennken.lennkenfinanzas.xsurvey.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.lennken.lennkenfinanzas.db.models.Survey;

import java.lang.reflect.Type;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class SurveySerializer implements JsonSerializer<Survey>{
    @Override
    public JsonElement serialize(Survey src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", src.getStatus());
        jsonObject.addProperty("endLatitude", src.getEndLatitude());
        jsonObject.addProperty("endLongitude", src.getEndLongitude());
        jsonObject.addProperty("idSurvey", src.getIdSurvey());
        jsonObject.addProperty("startLatitude", src.getStartLatitude());
        jsonObject.addProperty("startLongitude", src.getStartLongitude());
        jsonObject.addProperty("title", src.getTitle());
        jsonObject.add("questions", context.serialize(src.getQuestions()));
        return jsonObject;
    }
}
