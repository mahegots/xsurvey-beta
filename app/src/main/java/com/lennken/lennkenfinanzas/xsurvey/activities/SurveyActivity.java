package com.lennken.lennkenfinanzas.xsurvey.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.activities.BaseActivity;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.Survey;
import com.lennken.lennkenfinanzas.events.logic.EventLocationChanged;
import com.lennken.lennkenfinanzas.xsurvey.adapters.QuestionAdapter;
import com.lennken.lennkenfinanzas.xsurvey.enums.SurveyStatus;
import com.lennken.lennkenfinanzas.xsurvey.events.BarcodeEvent;
import com.lennken.lennkenfinanzas.xsurvey.events.CoordinateEvent;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventImageCaptured;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventLaunchSurvey;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.TableEvent;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventCaptureCoordinateClick;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventCaptureImageClick;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventOpenTable;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventPreviewCoordinateClick;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventPreviewImageClick;
import com.lennken.lennkenfinanzas.xsurvey.utils.Adapters;
import com.lennken.lennkenfinanzas.xsurvey.utils.CameraUtils;
import com.lennken.lennkenfinanzas.xsurvey.utils.RecyclerScroll;
import com.lennken.lennkenfinanzas.xsurvey.utils.Utils;

import java.io.ByteArrayOutputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */

public class SurveyActivity extends BaseActivity {

    @Bind(R.id.act_survey_save)
    FloatingActionButton saveButton;
    @Bind(R.id.act_survey_toolbar_container)
    LinearLayout toolbarLayout;

    int toolbarHeight;
    int fabMargin;
    boolean fadeToolbar = true;
    @Bind(R.id.act_survey_recycler)
    RecyclerView recyclerView;
    private QuestionAdapter mQuestionAdapter;
    private Question mQuestion;
    private Survey mSurvey;
    private Schema mSchema;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        ButterKnife.bind(this);
        Adapters.init(getApplicationContext());
        mQuestionAdapter = new QuestionAdapter(this, getSupportFragmentManager(), new RealmList<Question>());
        setupLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mQuestionAdapter.register();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mQuestionAdapter.unregister();
    }

    private void setupLayout() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.simple_grow);

        //  FAB margin needed for animation
        fabMargin = getResources().getDimensionPixelSize(R.dimen.fab_margin);
        toolbarHeight = Utils.getToolbarHeight(this);

        /* Set top padding= toolbar height.
         So there is no overlap when the toolbar hides.
         Avoid using 0 for the other parameters as it resets padding set via XML!*/
        recyclerView.setPadding(recyclerView.getPaddingLeft(), toolbarHeight,
                recyclerView.getPaddingRight(), recyclerView.getPaddingBottom());

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mQuestionAdapter);
        recyclerView.addOnScrollListener(new RecyclerScroll() {
            @Override
            public void show() {
                toolbarLayout.animate().translationY(0)
                        .setInterpolator(new DecelerateInterpolator(2)).start();
                if (fadeToolbar)
                    toolbarLayout.animate().alpha(1)
                            .setInterpolator(new DecelerateInterpolator(1)).start();
                saveButton.animate().translationY(0)
                        .setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                toolbarLayout.animate().translationY(-toolbarHeight)
                        .setInterpolator(new AccelerateInterpolator(2)).start();
                if (fadeToolbar)
                    toolbarLayout.animate().alpha(0)
                            .setInterpolator(new AccelerateInterpolator(1)).start();
                saveButton.animate().translationY(saveButton.getHeight() + fabMargin)
                        .setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        saveButton.startAnimation(animation);
    }

    @Subscribe(sticky = true)
    public void onLaunchSurvey(EventLaunchSurvey event) {
        mBus.removeStickyEvent(event);
        mSurvey = event.survey;
        mSchema = event.schema;
        mQuestionAdapter.setQuestionsList(mSurvey.getQuestions());
        mQuestionAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onCaptureImageEvent(EventCaptureImageClick event) {
        mQuestion = event.question;
        CameraUtils.takePhoto(this);
    }

    @Subscribe
    public void onPreviewImageEvent(EventPreviewImageClick event) {
        Intent intent = new Intent(this, PreviewImageActivity.class);
        mBus.postSticky(event.question);
        startActivity(intent);
    }

    @Subscribe
    public void onPreviewCoordinateEvent(EventPreviewCoordinateClick event) {
        mQuestion = event.question;
        mBus.postSticky(new CoordinateEvent(mQuestion));
        startActivity(new Intent(this, MapsActivity.class));
    }

    @Subscribe
    public void onCaptureCoordinateEvent(EventCaptureCoordinateClick event) {
        mQuestion = event.question;
        mBus.postSticky(new CoordinateEvent(mQuestion));
        startActivity(new Intent(this, MapsActivity.class));
    }

    @Subscribe
    public void onCaptureImageEvent(BarcodeEvent event) {


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CameraUtils.REQUEST_TAKE_PHOTO) {
            if (resultCode == RESULT_OK) {
                ProgressDialog progressDialog = ProgressDialog
                        .show(this, "", getString(R.string.loading), true);
                progressDialog.show();
                Bitmap bitmap;
                if ((bitmap = CameraUtils.fullBitmap()) != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
                    byte[] byteArray = stream.toByteArray();
                    saveInBase(new EventQuestionResponse(mQuestion,
                            Base64.encodeToString(byteArray, Base64.DEFAULT)));
                    progressDialog.dismiss();
                    mBus.post(new EventImageCaptured(mQuestion));
                } else {
                    progressDialog.dismiss();
                    Snackbar.make(getView(), R.string.error_capturing_image, Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onLocationReceived(EventLocationChanged event) {
        if (event.latLng != null) {
            String lat = String.valueOf(event.latLng.latitude);
            String lon = String.valueOf(event.latLng.longitude);
            Realm realm = Realm.getInstance(this);
            realm.beginTransaction();
            if (mSurvey.getStartLatitude() == null && mSurvey.getStartLongitude() != null) {
                mSurvey.setStartLatitude(lat);
                mSurvey.setStartLongitude(lon);
                mSchema.setLatitude(Double.valueOf(lat));
                mSchema.setLongitude(Double.valueOf(lon));

            }
            mSurvey.setEndLatitude(lat);
            mSurvey.setEndLongitude(lon);

            realm.commitTransaction();
            realm.close();
        }
    }

    @Subscribe
    public void onOpenTableClick(EventOpenTable event) {
        mQuestion = event.question;
        mBus.postSticky(new TableEvent(mQuestion));
        startActivity(new Intent(this, TableActivity.class));
    }

    @OnClick(R.id.act_survey_save)
    void onSaveClick() {
        Realm realm = Realm.getInstance(this);
        realm.beginTransaction();
        boolean surveyCompleted = true;
        for (Question question : mSurvey.getQuestions()) {
            if (question.isRequired()) {
                if (question.getResponse().trim().isEmpty()) {
                    Log.d(getClass().getName(), "Question: " + question.getQuestion());
                    surveyCompleted = false;
                    break;
                }
            }
        }
        mSurvey.setStatus(surveyCompleted ? SurveyStatus.COMPLETE : SurveyStatus.INCOMPLETE);
        mSurvey = realm.copyToRealmOrUpdate(mSurvey);
        realm.commitTransaction();
        Dao.insert(this, mSurvey);
        Dao.insert(this, mSchema);
        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.incomplete_survey)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }

    public void saveInBase(EventQuestionResponse event) {
        EventBus.getDefault().removeAllStickyEvents();
        Realm realm = Realm.getInstance(SurveyActivity.this);
        realm.beginTransaction();
        event.question.setIdResponse(event.idResponse);
        event.question.setResponse(event.response);
        event.question.setArrayPosition(event.position);
        if (event.responseArray != null) {
            if (!event.remove) {
                event.responseArray = realm.copyToRealmOrUpdate(event.responseArray);
                event.question.getResponseArray().add(event.responseArray);
            } else {
                event.question.getResponseArray().remove(event.responseArray);
            }
        }
        realm.commitTransaction();
    }

}
