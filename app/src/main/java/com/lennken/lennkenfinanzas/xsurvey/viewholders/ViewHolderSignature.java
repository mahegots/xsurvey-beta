package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.SignatureEvent;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventImageCaptured;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventCaptureImageClick;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventPreviewImageClick;
import com.lennken.lennkenfinanzas.xsurvey.fragments.SignatureDialogFragment;

import java.io.ByteArrayOutputStream;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderSignature extends RecyclerView.ViewHolder implements View.OnClickListener{
    private final FragmentManager fragmentManager;
    public LinearLayout linearLayout;
    public TextView textView;

    public ViewHolderSignature(View v, FragmentManager fm) {
        super(v);
        this.fragmentManager = fm;
        v.findViewById(R.id.item_survey_image_capture).setOnClickListener(this);
        v.findViewById(R.id.item_survey_image_preview).setOnClickListener(this);
        v.findViewById(R.id.item_survey_image_clean).setOnClickListener(this);
        linearLayout = (LinearLayout) v.findViewById(R.id.item_survey_image_container);
        textView = (TextView) v.findViewById(R.id.item_survey_image_title);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onClick(View view) {
        Question question = (Question) textView.getTag();
        switch (view.getId()){
            case R.id.item_survey_image_capture:
                SignatureDialogFragment.newInstance(question).show(fragmentManager,
                        "signature_dialog_fragment");
                break;
            case R.id.item_survey_image_preview:
                EventBus.getDefault().post(new EventPreviewImageClick(question));
                break;
            case R.id.item_survey_image_clean:
                EventBus.getDefault().post(new EventQuestionResponse(question, ""));
                linearLayout.setVisibility(View.GONE);
                break;
        }
    }

    @Subscribe
    public void onEventCapturedImage(SignatureEvent event){
        Question question = (Question) textView.getTag();
        if(event.question.getIdQuestion() == question.getIdQuestion()){
            linearLayout.setVisibility(View.VISIBLE);
        }else{
            linearLayout.setVisibility(View.GONE);
        }
    }
}