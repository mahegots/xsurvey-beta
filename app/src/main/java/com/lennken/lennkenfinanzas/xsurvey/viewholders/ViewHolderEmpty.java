package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderEmpty extends RecyclerView.ViewHolder {
    public ViewHolderEmpty(View v) {
        super(v);
    }
}
