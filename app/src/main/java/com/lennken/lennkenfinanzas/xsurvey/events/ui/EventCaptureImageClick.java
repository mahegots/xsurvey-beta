package com.lennken.lennkenfinanzas.xsurvey.events.ui;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by angelgomez on 7/28/15.
 */
public class EventCaptureImageClick {

    public final Question question;

    public EventCaptureImageClick(Question question) {
        this.question = question;
    }
}
