package com.lennken.lennkenfinanzas.xsurvey.events.ui;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by Daniel García Alvarado on 8/25/15.
 * LennkenFinanzas - danielgarcia
 */
public class EventPreviewCoordinateClick {
    public Question question;

    public EventPreviewCoordinateClick(Question question) {
        this.question = question;
    }
}
