package com.lennken.lennkenfinanzas.xsurvey.events;

import com.lennken.lennkenfinanzas.db.models.Answer;

/**
 * Created by MobileDev on 28/08/15.
 */
public class EventAnswerResponse {
    public Answer answer;
    public String response;

    public EventAnswerResponse(Answer answer, String response) {
        this.answer = answer;
        this.response = response;
    }
}
