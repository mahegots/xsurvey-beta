package com.lennken.lennkenfinanzas.xsurvey.enums;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
public class SurveyStatus {
    public static final int COMPLETE = 1;
    public static final int INCOMPLETE = 2;
}
