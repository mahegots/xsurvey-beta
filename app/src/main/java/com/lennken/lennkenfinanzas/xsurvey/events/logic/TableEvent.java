package com.lennken.lennkenfinanzas.xsurvey.events.logic;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by MobileDev on 28/08/15.
 */

public class TableEvent {
    public Question question;

    public TableEvent(Question question) {
        this.question = question;
    }
}
