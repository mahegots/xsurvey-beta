package com.lennken.lennkenfinanzas.xsurvey.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.activities.BaseActivity;
import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Response;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventAnswer;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.TableEvent;
import com.lennken.lennkenfinanzas.xsurvey.utils.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.Subscribe;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel García Alvarado on 8/27/15.
 * LennkenFinanzas - danielgarcia
 */
public class TableActivity extends BaseActivity {


    @Bind(R.id.act_table_table)
    TableLayout mTableLayout;
    private Question mQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        ButterKnife.bind(this);
    }

    @Subscribe(sticky = true)
    public void onLaunchTable(TableEvent event) {
        mBus.removeStickyEvent(event);
        mQuestion = event.question;
        initTable();
    }

    @Subscribe(sticky = true)
    public void onEventAnswerAdded(EventQuestionResponse event) {
        mBus.removeAllStickyEvents();
        Realm realm = Realm.getInstance(this);
        realm.beginTransaction();
        event.question.setIdResponse(event.idResponse);
        event.question.setResponse(event.response);
        event.question.setArrayPosition(event.position);
        event.question.getResponseArray().add(event.responseArray);
        mQuestion = event.question;
        realm.commitTransaction();
        initTable();
    }

    private void initTable() {
        int px = Utils.dpToPx(this, 8);
//        int maxPx = Utils.dpToPx(this, 0);
        mTableLayout.removeAllViews();
        TableRow headers = new TableRow(this);
        RealmList<Answer> answers = mQuestion.getAnswers();
        ArrayList<Integer> positions = new ArrayList<>();
        for (int i = 0; i < answers.size(); i++) {
            Answer answer = answers.get(i);
            TextView textView = new TextView(this);
            textView.setText(answer.getTitle());
            textView.setPadding(px, px, px, px);
            textView.setGravity(Gravity.CENTER);
//            textView.setMaxWidth(maxPx);
            textView.setBackgroundResource(R.drawable.cell_border);
            headers.addView(textView);

            positions.add(answer.getAnswerId());
        }

        mTableLayout.addView(headers);
        // TODO: Add answers to table
        try {
//            Log.e("Response:" , String.valueOf(mQuestion.getResponseArray().size()));
            for (Response response : mQuestion.getResponseArray()) {
                if (response.getResponse() != null && !response.getResponse().isEmpty()) {
                    Type listType = new TypeToken<RealmList<Response>>() {
                    }.getType();
                    RealmList<Response> responseRealmList = Utils.gsonForSerialization().fromJson(response.getResponse(), listType);

                    TableRow row = new TableRow(this);
                    HashMap<Integer, TextView> rows = new HashMap<>();
                    for (int i = 0; i < responseRealmList.size(); i++) {
                        Response r = responseRealmList.get(i);
                        TextView textView = new TextView(this);
                        textView.setText(r.getResponse());
//                        Log.e("Response:"+i , r.getResponse());
                        textView.setPadding(px, px, px, px);
                        textView.setGravity(Gravity.CENTER);
                        textView.setBackgroundResource(R.drawable.cell_border);
                        rows.put(r.getIdResponse(), textView);
                    }
                    for (Integer integer : positions) {
                        row.addView(rows.get(integer));
                    }
                    rows.clear();
                    mTableLayout.addView(row);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.act_table_add_response)
    public void OnClickAdd(View v) {
        mBus.postSticky(new EventAnswer(mQuestion, mQuestion.getAnswers()));
        startActivity(new Intent(this, TableAddElement.class));
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.incomplete_survey)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }
}
