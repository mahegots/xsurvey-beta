package com.lennken.lennkenfinanzas.xsurvey.utils;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Question;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class Adapters {

    static volatile Adapters singleton;
    public HashMap<Integer, ArrayAdapter<String>> adapterHashMap;
    public Context context;

    public static void init(Context context){
        synchronized (Adapters.class){
            singleton = new Adapters();
            singleton.adapterHashMap = new HashMap<>();
            singleton.context = context;
        }
    }

    public static ArrayAdapter<String> getAdapter(Question question) {
        if(!singleton.adapterHashMap.containsKey(question.getIdQuestion())) {
            final ArrayList<String> possibleAnswers = new ArrayList<>();
            for (Answer answer : question.getAnswers()) {
                possibleAnswers.add(answer.getTitle());
            }
            singleton.adapterHashMap.put(question.getIdQuestion(), new ArrayAdapter<>(
                    singleton.context,
                    R.layout.simple_spinner_dropdown_item,
                    possibleAnswers
            ));
        }
        return singleton.adapterHashMap.get(question.getIdQuestion());
    }
}
