package com.lennken.lennkenfinanzas.xsurvey.events;

import android.graphics.Bitmap;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by angelgomez on 7/28/15.
 */
public class SignatureEvent {

    public final Question question;

    public SignatureEvent(Question question) {
        this.question = question;
    }
}
