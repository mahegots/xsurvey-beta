package com.lennken.lennkenfinanzas.xsurvey.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.zxing.ResultMetadataType;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.BarcodeEvent;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

@SuppressLint("ValidFragment")
public class MultiScannerFragment extends DialogFragment implements ZXingScannerView.ResultHandler {
    private final Question question;
    private ZXingScannerView mScannerView;

    private ArrayList<Integer> mSelectedIndices;

    public MultiScannerFragment(Question question) {
        this.question = question;
    }

    private String mCodebarValue;

    private static final Collection <ResultMetadataType> DISPLAYABLE_METADATA_TYPES=
            EnumSet.of(
                    ResultMetadataType.ISSUE_NUMBER,
                    ResultMetadataType.SUGGESTED_PRICE,
                    ResultMetadataType.ERROR_CORRECTION_LEVEL,
                    ResultMetadataType.POSSIBLE_COUNTRY
                    );
    public static MultiScannerFragment newInstance(Question question) {
        return new MultiScannerFragment(question);
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics mMetrics = new DisplayMetrics();
        display.getMetrics(mMetrics);

        int mWidthFull = mMetrics.widthPixels;
        int mWidth = (int) Math.round(mWidthFull * .9);
        int mHeight = (int) Math.round(mWidth * .75);


        mScannerView = new ZXingScannerView(getActivity());

        setupFormats();

        mScannerView.setLayoutParams(new LinearLayout.LayoutParams(mWidth, mHeight));
        mBuilder.setView(mScannerView)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MultiScannerFragment.this.getDialog().cancel();
                    }
                });

        return mBuilder.create();

    }
/*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialog);
    }*/

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }


    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(com.google.zxing.Result rawResult) {

        try{
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getActivity(), notification );
        }catch(Exception e){
            e.printStackTrace();
        }

        Map<ResultMetadataType,Object> metadata = rawResult.getResultMetadata();

        if(metadata != null){
            StringBuilder metadataText = new StringBuilder(20);
            for(Map.Entry<ResultMetadataType, Object> entry : metadata.entrySet()){
                if(DISPLAYABLE_METADATA_TYPES.contains(entry.getKey())){
                   metadataText.append(entry.getValue()).append('\n');
                   //metadataText.append(entry.getValue());
                }
            }

            if (metadataText.length() > 3 || metadataText.length() > 4){
                //String mMetadataSignedRemoved = metadataText.substring(1);
                metadataText.setLength(metadataText.length() - 3);
                setmCodebarValue(metadataText + " " + rawResult.getText());
                showMessage(getmCodebarValue());
                mScannerView.stopCamera();

                EventBus.getDefault()
                        .post(new EventQuestionResponse(question, getmCodebarValue()));
                EventBus.getDefault().post(new BarcodeEvent(question, getmCodebarValue()));


                MultiScannerFragment.this.getDialog().cancel();
                //metadataText.setLength(metadataText.length() -1);
            }else{
                mScannerView.startCamera();
                showMessage("Enfoque la camara en el código de barras");
            }
        }
    }

    public void setupFormats(){
        List<com.google.zxing.BarcodeFormat> formats = new ArrayList<>();
        if(mSelectedIndices == null || mSelectedIndices.isEmpty()){
            mSelectedIndices = new ArrayList<Integer>();

            for(int i = 0; i < ZXingScannerView.ALL_FORMATS.size(); i++){
                mSelectedIndices.add(i);
            }
        }
            for(int index : mSelectedIndices){
                formats.add(ZXingScannerView.ALL_FORMATS.get(index));
            }

        if(mScannerView != null){
            mScannerView.setFormats(formats);
        }
    }

    private void showMessage(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    public String getmCodebarValue() {
        return mCodebarValue;
    }

    public void setmCodebarValue(String mCodebarValue) {
        this.mCodebarValue = mCodebarValue;
    }
}
