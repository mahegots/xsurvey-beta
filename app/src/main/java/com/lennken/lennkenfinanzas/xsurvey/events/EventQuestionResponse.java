package com.lennken.lennkenfinanzas.xsurvey.events;

import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.db.models.Response;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventQuestionResponse {

    public Response responseArray;
    public Question question;
    public boolean remove;
    public String response;
    public int idResponse;
    public int position;

    public EventQuestionResponse(Question question, String response){
        this.question = question;
        this.response = response;
        idResponse = -1;
    }

    public EventQuestionResponse(Question question, int idResponse){
        this.question = question;
        this.idResponse = idResponse;
    }

    public EventQuestionResponse(Question question, Response response){
        this.question = question;
        this.idResponse = -1;
        this.responseArray = response;
        this.response = "";
    }

    public EventQuestionResponse(Question question, Response response, boolean remove){
        this.question = question;
        this.remove = remove;
        this.response = "";
        this.idResponse = -1;
        this.responseArray = response;
    }
}
