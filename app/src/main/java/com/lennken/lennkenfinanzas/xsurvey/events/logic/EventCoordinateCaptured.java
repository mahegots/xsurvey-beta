package com.lennken.lennkenfinanzas.xsurvey.events.logic;

import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventPreviewImageClick;

/**
 * Created by Daniel García Alvarado on 8/25/15.
 * LennkenFinanzas - danielgarcia
 */
public class EventCoordinateCaptured {

    public Question question;

    public EventCoordinateCaptured(Question question){
        this.question = question;
    }
}
