package com.lennken.lennkenfinanzas.xsurvey.events;

/**
 * Created by angelgomez on 8/5/15.
 */
public class QuestionListAnswerEvent {

    public final int optionPosition;
    public final int listPosition;

    public QuestionListAnswerEvent(int optionPosition, int listPosition) {
        this.optionPosition = optionPosition;
        this.listPosition = listPosition;
    }
}
