package com.lennken.lennkenfinanzas.xsurvey.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Response;
import com.lennken.lennkenfinanzas.xsurvey.enums.AnswerType;
import com.lennken.lennkenfinanzas.xsurvey.events.EventAnswerResponse;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderEmpty;
import com.lennken.lennkenfinanzas.xsurvey.viewholders.ViewHolderOpenEnded;

import java.util.HashMap;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by MobileDev on 28/08/15.
 */
public class AnswerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TAG = "answers_adapter";
    private final AppCompatActivity activity;
    private final FragmentManager fm;
    private HashMap<String, Response> responseHashMap;
    private RealmList<Answer> answers;

    public HashMap<String, Response> getResponseHashMap(){
        return responseHashMap;
    }

    public AnswerAdapter(AppCompatActivity activity, FragmentManager fm, RealmList<Answer> answers,
                         HashMap<String, Response> responseHashMap) {
        this.activity = activity;
        this.answers = answers;
        this.fm = fm;
        this.responseHashMap = responseHashMap;
        EventBus.getDefault().register(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case (AnswerType.ROW): {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list_open_ended, parent, false);
                return new ViewHolderOpenEnded(v);
            }
            default:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_survey_list, parent, false);
                return new ViewHolderEmpty(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return answers.get(position).getType();
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Answer answer = answers.get(position);
        switch (holder.getItemViewType()) {
            //region OPEN_ENDED
            case (AnswerType.ROW): {
                final ViewHolderOpenEnded holderOpenEnded = (ViewHolderOpenEnded) holder;
                holderOpenEnded.editText.setTag(answer);
                holderOpenEnded.editText.setVisibility(View.VISIBLE);
                holderOpenEnded.editText.setHint(answer.getTitle());
                holderOpenEnded.editText.setFloatingLabelText(answer.getTitle());
                // holderOpenEnded.editText.setText(answer.get());
                break;
            }
            //endregion

        }
    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void onEventAnswerResponse(EventAnswerResponse event) {
        EventBus.getDefault().removeStickyEvent(event);
        responseHashMap.put(event.answer.getUuid(),
                new Response(event.response, event.answer.getAnswerId()));
    }
}
