package com.lennken.lennkenfinanzas.xsurvey.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.activities.BaseActivity;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventPreviewImageClick;
import com.lennken.lennkenfinanzas.xsurvey.utils.TouchImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.Subscribe;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
public class PreviewImageActivity extends BaseActivity{

    @Bind(R.id.act_preview_image)
    TouchImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ButterKnife.bind(this);
    }

    @Subscribe(sticky = true)
    public void onInit(Question question){
        mBus.removeStickyEvent(question);
        byte[] decodedString = Base64.decode(question.getResponse(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageView.setImageBitmap(decodedByte);
    }
}
