package com.lennken.lennkenfinanzas.xsurvey.events.logic;

/**
 * Created by angelgomez on 8/6/15.
 */
public class LocationEvent {

    public final String locationString;
    public final int position;

    public LocationEvent(String locationString, int position) {
        this.locationString = locationString;
        this.position = position;
    }
}
