package com.lennken.lennkenfinanzas.xsurvey.events;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by MobileDev on 02/09/15.
 */
public class BarcodeEvent {
    public Question question;
    public String barcode;

    public BarcodeEvent(Question question, String barcode) {
        this.question = question;
        this.barcode = barcode;
    }
}
