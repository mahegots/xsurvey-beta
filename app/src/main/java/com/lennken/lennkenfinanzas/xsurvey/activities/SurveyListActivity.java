package com.lennken.lennkenfinanzas.xsurvey.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.activities.BaseActivity;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.Survey;
import com.lennken.lennkenfinanzas.db.models.WorkPlan;
import com.lennken.lennkenfinanzas.events.logic.EventLaunchSchema;
import com.lennken.lennkenfinanzas.xsurvey.adapters.SurveyAdapter;
import com.lennken.lennkenfinanzas.xsurvey.enums.SchemaStatus;
import com.lennken.lennkenfinanzas.xsurvey.enums.SurveyStatus;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.EventLaunchSurvey;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventSurveyClicked;
import com.lennken.lennkenfinanzas.xsurvey.services.SyncService;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.Subscribe;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class SurveyListActivity extends BaseActivity {

    @Bind(R.id.act_survey_list_recycler)
    RecyclerView recyclerView;
    private SurveyAdapter mSurveyAdapter;
    private Schema mSchema;
    private WorkPlan mWorkPlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_list);
        ButterKnife.bind(this);
        mSurveyAdapter = new SurveyAdapter(this, new RealmList<Survey>());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mSurveyAdapter);
    }

    @Subscribe(sticky = true)
    public void onLaunchSchema(EventLaunchSchema event) {
        mBus.removeStickyEvent(event);
        mSchema = event.schema;
        mWorkPlan = event.workPlan;
        reloadSurveys();
    }

    private void reloadSurveys() {
        mSurveyAdapter.setSurveys(mSchema.getSurveys());
        mSurveyAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadSurveys();
    }

    @Subscribe
    public void onSurveyEvent(EventSurveyClicked event) {
        Intent intent = new Intent(this, SurveyActivity.class);
        mBus.postSticky(new EventLaunchSurvey(event.survey, mSchema));
        startActivity(intent);
    }

    @OnClick(R.id.act_survey_list_done) void onDoneClick(){
        boolean availableToSync = true;
        for(Survey survey : mSchema.getSurveys()){
            if(survey.isRequired()){
                if (survey.getStatus() == SurveyStatus.INCOMPLETE){
                    availableToSync = false;
                    break;
                }
            }
        }
        if(availableToSync){
            Realm realm = Realm.getInstance(this);
            realm.beginTransaction();
            mSchema.setStatus(SchemaStatus.COMPLETE);
            mSchema = realm.copyToRealmOrUpdate(mSchema);
            realm.commitTransaction();
            SyncService.startSyncService(this);
        }
        Dao.insert(this, mSchema);
        Dao.insert(this, mWorkPlan);
        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.incomplete_survey)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }
}
