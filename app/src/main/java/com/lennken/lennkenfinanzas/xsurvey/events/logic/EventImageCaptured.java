package com.lennken.lennkenfinanzas.xsurvey.events.logic;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventImageCaptured {
    public Question question;

    public EventImageCaptured(Question question){
        this.question = question;
    }
}
