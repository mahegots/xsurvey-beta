package com.lennken.lennkenfinanzas.xsurvey.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Survey;
import com.lennken.lennkenfinanzas.xsurvey.enums.SurveyStatus;
import com.lennken.lennkenfinanzas.xsurvey.events.ui.EventSurveyClicked;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import io.realm.RealmList;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.ViewHolder>{

    private final Context context;
    private RealmList<Survey> surveys;

    public SurveyAdapter(Context context, RealmList<Survey> surveys) {
        this.surveys = surveys;
        this.context = context;
    }

    public void setSurveys(RealmList<Survey> surveys) {
        this.surveys = surveys;
    }

    @Override
    @SuppressLint("InflateParams")
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_survey, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Survey survey = surveys.get(position);
        holder.surveyNameTextView.setText(survey.getTitle());
        // TODO: Calculate correct completed
        holder.completedTextView.setText(survey.getStatus() == SurveyStatus.COMPLETE
                ? R.string.completed : R.string.incomplete);
        holder.completedTextView.setTextColor(context.getResources()
                .getColor(survey.getStatus() == SurveyStatus.COMPLETE
                ? R.color.teal : R.color.navy));
        holder.requiredTextView.setText(survey.isRequired()
                ? R.string.required : R.string.not_required);
        holder.requiredTextView.setTextColor(context.getResources().getColor(survey.isRequired()
                ? R.color.red : R.color.primary));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventSurveyClicked((Survey)view.getTag()));
            }
        });
        holder.cardView.setTag(survey);
    }

    @Override
    public int getItemCount() {
        return surveys.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.item_survey_name)
        TextView surveyNameTextView;
        @Bind(R.id.item_survey_completed)
        TextView completedTextView;
        @Bind(R.id.item_survey_required)
        TextView requiredTextView;
        @Bind(R.id.card_view)
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
