package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Answer;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.enums.AnswerType;
import com.lennken.lennkenfinanzas.xsurvey.events.EventQuestionResponse;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;

import de.greenrobot.event.EventBus;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderRadioGroup extends RecyclerView.ViewHolder {
    public TextView textView;
    public RadioGroup radioGroup;
    public MaterialEditText editText;
    public HashMap<String, CheckedChangeImpl> checkedChangeHashMap;

    public ViewHolderRadioGroup(View v) {
        super(v);
        radioGroup = (RadioGroup) v.findViewById(R.id.item_survey_list_radio_group);
        textView = (TextView) v.findViewById(R.id.item_survey_list_list_title);
        editText = (MaterialEditText) v.findViewById(R.id.item_survey_list_edit_text);
        checkedChangeHashMap = new HashMap<>();
    }

    public CheckedChangeImpl getCheckedChangeListener(RadioButton radioButton, Question question,
                                                      int position){
        Answer answer = (Answer) radioButton.getTag();
        if(!checkedChangeHashMap.containsKey(answer.getUuid())){
            checkedChangeHashMap.put(answer.getUuid(), new CheckedChangeImpl(this, question, position));
        }
        return checkedChangeHashMap.get(answer.getUuid());
    }

    public class CheckedChangeImpl implements CompoundButton.OnCheckedChangeListener{
        private ViewHolderRadioGroup vh;
        private Question question;
        private int position;

        public CheckedChangeImpl(ViewHolderRadioGroup vh, Question question, int position){
            this.vh = vh;
            this.question = question;
            this.position = position;
        }

        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if(question != null) {
                final Answer answerTag = (Answer) compoundButton.getTag();
                if (answerTag.getType() == AnswerType.OTHER) {
                    if (b) {
                        vh.editText.setVisibility(View.VISIBLE);
                        vh.editText.setText("");
                        final EventQuestionResponse event = new EventQuestionResponse(
                                question, vh.editText.getText().toString()
                        );
                        event.idResponse = answerTag.getAnswerId();
                        EventBus.getDefault().post(event);
                        vh.editText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i,
                                                          int i1, int i2) {
                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i,
                                                      int i1, int i2) {
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                event.response = editable.toString();
                                event.idResponse = answerTag.getAnswerId();
                                event.position = position;
                                EventBus.getDefault().post(event);
                            }
                        });
                    } else {
                        vh.editText.setVisibility(View.GONE);
                        EventQuestionResponse event = new EventQuestionResponse(question,
                                answerTag.getTitle());
                        event.idResponse = answerTag.getAnswerId();
                        EventBus.getDefault().post(event);
                    }
                } else {
                    EventQuestionResponse event = new EventQuestionResponse(question, "");
                    if (b) {
                        event.response = answerTag.getTitle();
                        event.idResponse = answerTag.getAnswerId();
                        event.position = position;
                        EventBus.getDefault().post(event);
                        Log.d(getClass().getName(), "Position saved: "+position);
                    }
                }
            }
        }
    }
}
