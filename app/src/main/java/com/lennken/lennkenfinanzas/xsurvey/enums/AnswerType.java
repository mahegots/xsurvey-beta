package com.lennken.lennkenfinanzas.xsurvey.enums;

/**
 * Created by angelgomez on 7/21/15.
 */
public class AnswerType {

    public static final int ROW = 1;
    public static final int RADIO = 2;
    public static final int OTHER = 3;

}
