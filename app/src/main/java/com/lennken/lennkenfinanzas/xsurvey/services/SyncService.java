/*
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennken.lennkenfinanzas.xsurvey.services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.lennken.lennkenfinanzas.LnkApp;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.User;
import com.lennken.lennkenfinanzas.services.LocationService;
import com.lennken.lennkenfinanzas.ws.requests.SurveyRequest;
import com.lennken.lennkenfinanzas.ws.responses.SurveyResponse;
import com.lennken.lennkenfinanzas.xsurvey.enums.SchemaStatus;
import com.lennken.lennkenfinanzas.xsurvey.events.logic.SyncEvents;
import com.lennken.lennkenfinanzas.xsurvey.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.RetrofitError;

/**
 * A utility IntentService, used for a variety of asynchronous background
 * operations that do not necessarily need to be tied to a UI.
 */
public class SyncService extends IntentService {
    private static final String TAG = SyncService.class.getSimpleName();

    private static final String ACTION_SYNC = "sync_surveys";
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;

    public SyncService() {
        super(TAG);
    }

    public static void startSyncService(Context context) {
        Intent intent = new Intent(context, SyncService.class);
        intent.setAction(SyncService.ACTION_SYNC);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        if (ACTION_SYNC.equals(action)) {
            sync();
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void sync() {
        EventBus.getDefault().post(new SyncEvents.SyncStartEvent());
        User user = Dao.user(this);
        Realm realm = Realm.getInstance(this);
        realm.beginTransaction();
        startSync(1);

        RealmResults<Schema> schemas = realm.where(Schema.class).equalTo("status",
                SchemaStatus.COMPLETE).findAll();
        RealmList<Schema> schemaRealmList = new RealmList<>();
        for (Schema schema : schemas) {
            schemaRealmList.add(schema);
        }
        for (Schema schema : schemaRealmList) {
            schema.setStatus(SchemaStatus.PENDING_SYNC);
            schema.setUsername(user.getUsername());
            schema.setPushId(user.getPushId());
            try {
                SurveyRequest surveyRequest = new SurveyRequest();
                surveyRequest.setPushId(user.getPushId().isEmpty() ? "unknown" : user.getPushId());
                surveyRequest.setUsername(user.getUsername());
                Gson gson = Utils.gsonForSerialization();
                surveyRequest.setContentJson(gson.toJson(schema));
                surveyRequest.setDateSync(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                surveyRequest.setId(schema.getUuid());
                surveyRequest.setIdVisit(String.valueOf(schema.getIdVisit()));
                surveyRequest.setIdWorkPlan(String.valueOf(schema.getIdWorkPlan()));
                surveyRequest.setIdModule(String.valueOf(schema.getSchemaVersion()));
                Log.e("Modile", surveyRequest.getIdModule());
                surveyRequest.setImei(user.getImei());
                double latitude = LocationService.getLastLocation().getLatitude();
                double longitude = LocationService.getLastLocation().getLongitude();
                surveyRequest.setLatitude(String.valueOf(latitude));
                surveyRequest.setLongitude(String.valueOf(longitude));

                surveyRequest.setSchemaVersion(schema.getSchemaVersion());

                File file = Environment.getExternalStorageDirectory();
                File json = new File(file, "/sync.json");
                try {
                    boolean created = json.createNewFile();
                    if (created) {
                        PrintWriter pr = new PrintWriter(new FileOutputStream(json));
                        pr.println(surveyRequest.getContentJson());
                        pr.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                SurveyResponse surveyResponse = LnkApp.getLnkService().survey("bearer " + user.getToken(),
                        surveyRequest);
                if (surveyResponse.isSuccess()) {
                    schema.setStatus(SchemaStatus.SYNCED);
                    if (schema.getVisit() != null) {
                        schema.getVisit().setVisited(true);
                        realm.copyToRealm(schema.getVisit());
                    }
                } else {
                    schema.setStatus(SchemaStatus.COMPLETE);
                }
            } catch (RetrofitError error) {
                Log.e("URL", error.getUrl());
                Log.d("Error", "" + error);
                Log.d("Error", "" + error.getMessage());
                schema.setStatus(SchemaStatus.COMPLETE);
                realm.copyToRealmOrUpdate(schema);
                EventBus.getDefault().post(new SyncEvents.SyncErrorEvent(error));
                realm.commitTransaction();
                realm.close();
                finishSync(false);
                return;
            } catch (ClassNotFoundException e) {
                Log.d("catch", "" + e);
                EventBus.getDefault().post(new SyncEvents.SyncErrorEvent(e));
                e.printStackTrace();
                realm.commitTransaction();
                realm.close();
                finishSync(false);
                return;
            }
            realm.copyToRealmOrUpdate(schema);
        }
        realm.commitTransaction();
        realm.close();
        finishSync(true);
        EventBus.getDefault().post(new SyncEvents.SyncFinishEvent());
    }


    private void startSync(int id) {
        mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setContentTitle("Lennken Encuestas")
                .setAutoCancel(true)
                .setOngoing(true)
                .setProgress(0, 0, true)
                .setContentText("Sincronizando...");
        mNotificationManager.notify(id, mBuilder.build());
    }

    private void finishSync(boolean status) {
        mNotificationManager.notify(1, mBuilder.build());
        mBuilder.setSmallIcon(status ? android.R.drawable.stat_sys_upload_done : android.R.drawable.stat_notify_error);
        mBuilder.setContentText(!status ? getString(R.string.error_sync) : getString(R.string.synced))
                .setProgress(0, 0, false);
        mBuilder.setOngoing(false);
        Intent intent = new Intent(this, SyncService.class);
        intent.setAction(SyncService.ACTION_SYNC);
        if (!status) {
            PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, 0);
            mBuilder.addAction(android.R.drawable.ic_popup_sync, "Reintentar", pendingIntent);
        }
        mNotificationManager.notify(1, mBuilder.build());
    }
}
