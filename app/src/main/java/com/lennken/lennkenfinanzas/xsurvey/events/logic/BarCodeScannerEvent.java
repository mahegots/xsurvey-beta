package com.lennken.lennkenfinanzas.xsurvey.events.logic;

/**
 * Created by OSCAR on 9/4/15.
 */
public class BarCodeScannerEvent {
    public String barcode;

    public BarCodeScannerEvent(String barcode) {
        this.barcode = barcode;
    }
}
