package com.lennken.lennkenfinanzas.xsurvey.events.ui;

import com.lennken.lennkenfinanzas.db.models.Question;

/**
 * Created by Daniel García Alvarado on 8/25/15.
 * LennkenFinanzas - danielgarcia
 */
public class EventCaptureCoordinateClick {
    public Question question;

    public EventCaptureCoordinateClick(Question question) {
        this.question = question;
    }
}
