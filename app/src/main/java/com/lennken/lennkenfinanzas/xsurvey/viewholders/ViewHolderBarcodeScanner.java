package com.lennken.lennkenfinanzas.xsurvey.viewholders;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Question;
import com.lennken.lennkenfinanzas.xsurvey.events.BarcodeEvent;
import com.lennken.lennkenfinanzas.xsurvey.fragments.MultiScannerFragment;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class ViewHolderBarcodeScanner extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final FragmentManager fragmentManager;
    public EditText ean2EditText;
    public TextView textView;

    public ViewHolderBarcodeScanner(View v, FragmentManager fm) {
        super(v);
        this.fragmentManager = fm;
        v.findViewById(R.id.item_survey_barcode_capture).setOnClickListener(this);
        ean2EditText = (EditText) v.findViewById(R.id.item_survey_barcode_ean2);
        textView = (TextView) v.findViewById(R.id.item_survey_barcode_title);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onClick(View view) {
        Question question = (Question) textView.getTag();
        switch (view.getId()) {
            case R.id.item_survey_barcode_capture:
                MultiScannerFragment.newInstance(question).show(fragmentManager,
                        "barcode_dialog_fragment");
                break;
        }
    }

    @Subscribe
    public void onEventBarcode(BarcodeEvent event) {
        Question question = (Question) textView.getTag();
        if(event.question.getUuid().equals(question.getUuid())){
            ean2EditText.setText(event.barcode);
        }

    }


}