package com.lennken.lennkenfinanzas.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.models.Visit;
import com.lennken.lennkenfinanzas.xsurvey.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmList;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class VisitDoneAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private final Context mContext;
    private RealmList<Visit> mVisitArrayList;

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_visited, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Visit visit = mVisitArrayList.get(position);
        holder.addressTextView.setText(visit.getAddressOne() + "\n" +
                visit.getCity() + "," + visit.getState());
        holder.contactTextView.setText(visit.getClientContact());
        holder.nameTextView.setText(visit.getClientName());
        holder.positionTextView.setText("#" + (position + 1));
        holder.sourceTextView.setText(visit.getIdVisit() == -1
                ? R.string.source_mobile : R.string.assigned);
        // TODO: Assign status color
        return convertView;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_header, null);
            holder = new HeaderViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        holder.text.setText(Utils.getDayDateString(mVisitArrayList.get(position).getSchedule()));
        holder.countText.setText("0/0");
        holder.countText.setVisibility(View.INVISIBLE);


        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        long sum = 0;
        try {
            char[] charArray = Utils.getDayDateString(mVisitArrayList.get(position).getSchedule())
                    .toCharArray();
            for (char aCharArray : charArray) {
                sum += aCharArray;
            }
        }catch (Exception ignored){}
        return sum;
    }

    public VisitDoneAdapter(Context context, RealmList<Visit> visits) {
        this.mContext = context;
        this.mVisitArrayList = visits;
    }

    @Override
    public int getCount() {
        return mVisitArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mVisitArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setVisits(RealmList<Visit> visits) {
        this.mVisitArrayList = visits;
    }

    static class ViewHolder {
        @Bind(R.id.item_prospecting_name)
        TextView nameTextView;
        @Bind(R.id.item_prospecting_contact)
        TextView contactTextView;
        @Bind(R.id.item_prospecting_address)
        TextView addressTextView;
        @Bind(R.id.item_prospecting_position)
        TextView positionTextView;
        @Bind(R.id.item_prospecting_source)
        TextView sourceTextView;
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class HeaderViewHolder {
        @Bind(R.id.item_header_text)
        TextView text;
        @Bind(R.id.item_header_counter)
        TextView countText;

        public HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
