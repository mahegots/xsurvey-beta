package com.lennken.lennkenfinanzas.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.lennken.lennkenfinanzas.fragments.ModuleCompletedFragment;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class CompletedTabAdapter extends FragmentStatePagerAdapter {

    public CompletedTabAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ModuleCompletedFragment.newInstance("originacion");
            case 1:
                return ModuleCompletedFragment.newInstance("preaprobacion");
            case 2:
                return ModuleCompletedFragment.newInstance("cobranza");
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return new String[]{"Originación", "Preaprobación", "Cobranza"}[position];
    }
}
