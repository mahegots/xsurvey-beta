package com.lennken.lennkenfinanzas.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.lennken.lennkenfinanzas.activities.LoginActivity;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class IntentUtils {

    public static void login(Context context, Integer message) {
        if (message != null) {
            Toast.makeText(context.getApplicationContext(),
                    message.intValue(), Toast.LENGTH_LONG).show();
        }
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void callPhone(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }

    public static void navigateTo(Context context, String address) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=" + address));
        context.startActivity(intent);
    }
}
