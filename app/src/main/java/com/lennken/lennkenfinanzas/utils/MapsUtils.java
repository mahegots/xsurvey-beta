package com.lennken.lennkenfinanzas.utils;

/**
 * Created by OSCAR on 8/26/15.
 */
public class MapsUtils {
    double lat;
    double lon;

    public MapsUtils(String coordinates) {
        String[] points = coordinates.split(",");
        lat = Double.valueOf(points[0]);
        lon = Double.valueOf(points[1]);
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
