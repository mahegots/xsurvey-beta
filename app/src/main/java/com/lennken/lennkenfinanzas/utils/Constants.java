package com.lennken.lennkenfinanzas.utils;

/**
 * Created by danielgarcia on 7/19/15.
 */
public class Constants {
    public static final long GOOGLE_API_CLIENT_TIMEOUT_S = 10;
    public static final String GOOGLE_API_CLIENT_ERROR_MSG =
            "Failed to connect to GoogleApiClient (error code = %d)";
    public static final String SENDER_ID = "731443648181";
    public static final String PREFS_NAME = "lnkPrefs";
}
