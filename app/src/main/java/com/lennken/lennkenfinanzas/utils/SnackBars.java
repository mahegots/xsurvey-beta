package com.lennken.lennkenfinanzas.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.lennken.lennkenfinanzas.R;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
public class SnackBars {
    public static void error(Context context, View view, int message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.red));
        snackbar.show();
    }

    public static void withActions(Context context, View view, int message, int[] actions,
                                   View.OnClickListener... clickListeners) {
        if(actions.length != clickListeners.length)
            throw new IllegalArgumentException("Number of actions and listeners isn't equal");
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.primary));
        snackbar.setActionTextColor(context.getResources().getColor(R.color.white));
        for(int i  = 0; i < actions.length; i++){
            int action = actions[i];
            snackbar.setAction(action, clickListeners[i]);
        }
        snackbar.show();
    }
}
