package com.lennken.lennkenfinanzas.events.logic;

import com.lennken.lennkenfinanzas.db.models.Schema;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventSchemaDownload {
    public Schema schema;
    public String error;

    public EventSchemaDownload(Schema schema, String error) {
        this.schema = schema;
        this.error = error;
    }
}
