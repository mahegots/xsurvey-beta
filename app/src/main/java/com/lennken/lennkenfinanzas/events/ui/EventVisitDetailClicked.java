package com.lennken.lennkenfinanzas.events.ui;

import com.lennken.lennkenfinanzas.db.models.Visit;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventVisitDetailClicked {
    public Visit visit;

    public EventVisitDetailClicked(Visit visit) {
        this.visit = visit;
    }
}
