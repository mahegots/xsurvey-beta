package com.lennken.lennkenfinanzas.events.logic;

import com.lennken.lennkenfinanzas.db.models.WorkPlan;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventWorkPlanDownloaded {

    public String error;
    public WorkPlan workPlan;

    public EventWorkPlanDownloaded(WorkPlan workPlan, String error) {
        this.workPlan = workPlan;
        this.error = error;
    }
}
