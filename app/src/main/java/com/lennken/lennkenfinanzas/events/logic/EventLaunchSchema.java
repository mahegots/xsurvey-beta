package com.lennken.lennkenfinanzas.events.logic;

import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.WorkPlan;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventLaunchSchema {
    public Schema schema;
    public WorkPlan workPlan;

    public EventLaunchSchema(Schema schema, WorkPlan workPlan) {
        this.schema = schema;
        this.workPlan = workPlan;
    }
}
