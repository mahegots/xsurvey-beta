package com.lennken.lennkenfinanzas.events.ui;

import com.lennken.lennkenfinanzas.db.models.Visit;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class EventNavigationClick {
    public Visit visit;

    public EventNavigationClick(Visit visit) {
        this.visit = visit;
    }
}
