package com.lennken.lennkenfinanzas.events.logic;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by danielgarcia on 7/19/15.
 */
public class EventLocationChanged {

    public LatLng latLng;

    public EventLocationChanged(LatLng latLng) {
        this.latLng = latLng;
    }
}
