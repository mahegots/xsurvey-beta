package com.lennken.lennkenfinanzas.ws.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniel García Alvarado on 8/19/15.
 * Lennken Finanzas - danielgarcia
 */
public class UserInfoResponse{

    @SerializedName("ClientCode")
    private String clientCode;
    @SerializedName("Phone")
    private String phone;

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
