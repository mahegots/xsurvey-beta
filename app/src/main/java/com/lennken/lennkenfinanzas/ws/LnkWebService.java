package com.lennken.lennkenfinanzas.ws;

import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.WorkPlan;
import com.lennken.lennkenfinanzas.ws.requests.DeviceRequest;
import com.lennken.lennkenfinanzas.ws.requests.SurveyRequest;
import com.lennken.lennkenfinanzas.ws.responses.DeviceResponse;
import com.lennken.lennkenfinanzas.ws.responses.LoginResponse;
import com.lennken.lennkenfinanzas.ws.responses.SurveyResponse;
import com.lennken.lennkenfinanzas.ws.responses.UserInfoResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Daniel García Alvarado on 8/19/15.
 * Lennken Finanzas - danielgarcia
 */
public interface LnkWebService{

    @FormUrlEncoded
    @POST("/oauth/token")
    void login(@Field("sourceLogin") String sourceLogin,
               @Field("grant_type") String grantType,
               @Field("username") String username,
               @Field("password") String password,
               Callback<LoginResponse> callback);

    @GET("/api/claims/user")
    void userInfo(@Header("Authorization") String authorization,
                  @Query("token") String token,
                  Callback<UserInfoResponse> callback);

    @GET("/api/workingplan")
    void workPlan(@Header("Authorization") String authorization,
                  Callback<WorkPlan> callback);

    @GET("/api/surveys")
    void schema(@Header("Authorization") String authorization,
                @Query("module") int schemaVersion,
                Callback<Schema> callback);

    @GET("/api/surveys/{module}")
    void mockSchema(@Header("Authorization") String authorization,
                    @Path("module") int schemaVersion,
                    Callback<Schema> callback);

    @POST("/api/devicelocation")
    void location(@Header("Authorization") String authorization,
                  @Body DeviceRequest deviceRequest,
                  Callback<DeviceResponse> callback);

    @POST("/api/surveys")
    SurveyResponse survey(@Header("Authorization") String authorization,
                          @Body SurveyRequest surveyRequest);

    @POST("/api/surveys")
    SurveyResponse survey(@Header("Authorization") String authorization,
                          @Query("model") String surveyRequest);

}
