package com.lennken.lennkenfinanzas.ws;

import android.content.Context;
import android.net.Uri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class MockClient implements Client {

    HashMap<String, String> pathsHashMap;
    private Context context;

    public MockClient(Context context){
        this.context = context;
        pathsHashMap = new HashMap<>();
    }

    public void addMockRoute(String route, String jsonName){
        pathsHashMap.put(route, jsonName);
    }

    @Override
    public Response execute(Request request) throws IOException {
        Uri uri = Uri.parse(request.getUrl());

        String jsonName = pathsHashMap.get(uri.getPath());
        BufferedReader reader = null;
        boolean processed = true;
        StringBuilder builder = new StringBuilder();
        Exception exception = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(jsonName)));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            processed = true;
        } catch (IOException e) {
            exception = e;
            processed = false;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    exception = e;
                    processed = false;
                }
            }
        }
        if (processed){
            return new Response(request.getUrl(),
                    200,
                    "Success",
                    new ArrayList<Header>(),
                    new TypedByteArray("application/json", builder.toString().getBytes()));
        }else{
            return new Response(request.getUrl(),
                    500,
                    exception.getMessage(),
                    new ArrayList<Header>(),
                    new TypedByteArray("application/json", builder.toString().getBytes()));
        }

    }
}
