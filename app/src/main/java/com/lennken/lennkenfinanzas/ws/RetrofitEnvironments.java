package com.lennken.lennkenfinanzas.ws;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.realm.RealmObject;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class RetrofitEnvironments {

    public static LnkWebService createQAEnvironment() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://lnkfinanzas.lennken.com")
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        return restAdapter.create(LnkWebService.class);
    }

    public static LnkWebService createMockEnvironment(Context context) {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();

        MockClient mockClient = new MockClient(context);
        mockClient.addMockRoute("/api/workingplan", "workPlan.json");
        mockClient.addMockRoute("/api/surveys/1", "schema1.json");
        mockClient.addMockRoute("/api/surveys/2", "schema2.json");
        mockClient.addMockRoute("/api/surveys/3", "schema3.json");
        mockClient.addMockRoute("/api/surveys/4", "schema4.json");

        RestAdapter.Builder restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://lnkfinanzas.lennken.com")
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(mockClient);
        return restAdapter.build().create(LnkWebService.class);
    }
}
