package com.lennken.lennkenfinanzas.ws.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniel García Alvarado on 8/22/15.
 * Lennken Finanzas - danielgarcia
 */
public class DeviceRequest {

    @SerializedName("Latitude")
    private String latitude;
    @SerializedName("Longitude")
    private String longitude;
    @SerializedName("Battery")
    private String battery;
    @SerializedName("Signal")
    private String signal;
    @SerializedName("EventCode")
    private String eventCode;
    @SerializedName("AppVersion")
    private String appVersion;
    @SerializedName("IMEI")
    private String imei;
    @SerializedName("UserName")
    private String username;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
