package com.lennken.lennkenfinanzas.ws.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class SurveyRequest {

    @SerializedName("Id")
    private String id;
    @SerializedName("DateSync")
    private String dateSync;
    @SerializedName("SchemaVersion")
    private int schemaVersion;
    @SerializedName("ContentJson")
    private String contentJson;
    @SerializedName("PushId")
    private String pushId;
    @SerializedName("UserName")
    private String username;
    @SerializedName("IdWorkPlan")
    private String idWorkPlan;
    @SerializedName("IdVisit")
    private String idVisit;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("IdModule")
    private String IdModule;
    @SerializedName("Imei")
    private String imei;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getIdModule() {
        return IdModule;
    }

    public void setIdModule(String idModule) {
        IdModule = idModule;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateSync() {
        return dateSync;
    }

    public void setDateSync(String dateSync) {
        this.dateSync = dateSync;
    }

    public int getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(int schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public String getContentJson() {
        return contentJson;
    }

    public void setContentJson(String contentJson) {
        this.contentJson = contentJson;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdWorkPlan() {
        return idWorkPlan;
    }

    public void setIdWorkPlan(String idWorkPlan) {
        this.idWorkPlan = idWorkPlan;
    }

    public String getIdVisit() {
        return idVisit;
    }

    public void setIdVisit(String idVisit) {
        this.idVisit = idVisit;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
