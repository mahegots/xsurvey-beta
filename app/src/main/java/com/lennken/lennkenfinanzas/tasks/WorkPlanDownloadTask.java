package com.lennken.lennkenfinanzas.tasks;

import android.app.ProgressDialog;
import android.content.Context;

import com.lennken.lennkenfinanzas.LnkApp;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.User;
import com.lennken.lennkenfinanzas.db.models.WorkPlan;
import com.lennken.lennkenfinanzas.events.logic.EventWorkPlanDownloaded;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class WorkPlanDownloadTask {

    private final Context context;
    private ProgressDialog progressDialog;

    public WorkPlanDownloadTask(Context context) {
        this.context = context;
        progressDialog = ProgressDialog.show(context, "", context.getString(R.string.loading), true);
    }

    public void execute() {
        progressDialog.show();
        User user = Dao.user(context);
        LnkApp.getLnkService().workPlan("bearer " + user.getToken(),
      //  LnkApp.getLnkMockService().workPlan("bearer " + user.getToken(),
                new Callback<WorkPlan>() {
                    @Override
                    public void success(WorkPlan workPlan, Response response) {
                        progressDialog.dismiss();
                        EventBus.getDefault().post(new EventWorkPlanDownloaded(workPlan,
                                response.getReason()));
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressDialog.dismiss();
                        EventBus.getDefault().post(new EventWorkPlanDownloaded(null,
                                error.getMessage()));
                    }
                });
    }
}
