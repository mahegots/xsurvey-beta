package com.lennken.lennkenfinanzas.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.lennken.lennkenfinanzas.LnkApp;
import com.lennken.lennkenfinanzas.R;
import com.lennken.lennkenfinanzas.db.Dao;
import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.User;
import com.lennken.lennkenfinanzas.events.logic.EventSchemaDownload;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class SchemaDownloadTask {

    private final Context context;
    private int schemaVersion;
    private ProgressDialog progressDialog;

    public SchemaDownloadTask(Context context, int schemaVersion) {
        this.context = context;
        this.schemaVersion = schemaVersion;
        progressDialog = ProgressDialog.show(context, "", context.getString(R.string.loading), true);
    }


    public void execute() {
        progressDialog.show();
        User user = Dao.user(context);
//        LnkApp.getLnkMockService().mockSchema("bearer " + user.getToken(),
        LnkApp.getLnkService().schema("bearer " + user.getToken(),
                schemaVersion, new Callback<Schema>() {
                    @Override
                    public void success(Schema schema, Response response) {
                        Log.e("URL", response.getUrl());
                        progressDialog.dismiss();
                        if (response.getStatus() == 200) {
                            EventBus.getDefault().post(new EventSchemaDownload(schema, null));
                        } else {
                            EventBus.getDefault().post(new EventSchemaDownload(null,
                                    response.getReason()));
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("URL", error.getUrl());
                        progressDialog.dismiss();
                        EventBus.getDefault().post(new EventSchemaDownload(null, error.getMessage()));
                    }
                });
    }
}
