package com.lennken.lennkenfinanzas.db;

import android.content.Context;

import com.lennken.lennkenfinanzas.db.models.Module;
import com.lennken.lennkenfinanzas.db.models.Schema;
import com.lennken.lennkenfinanzas.db.models.User;
import com.lennken.lennkenfinanzas.db.models.Visit;
import com.lennken.lennkenfinanzas.db.models.WorkPlan;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Daniel García Alvarado on 8/19/15.
 * Lennken Finanzas - danielgarcia
 */
public class Dao {
    public static <T extends RealmObject>T insert(Context context, T model) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        model = realm.copyToRealmOrUpdate(model);
        realm.commitTransaction();
        realm.close();
        return model;
    }

    public static User user(Context context) {
        Realm realm = Realm.getInstance(context);
        return realm.where(User.class).findFirst();
    }

    public static WorkPlan workPlan(Context context) {
        Realm realm = Realm.getInstance(context);
        return realm.where(WorkPlan.class).findFirst();
    }

    public static void saveWorkPlan(Context context, WorkPlan workPlan) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(workPlan);
        realm.commitTransaction();
        realm.close();
    }

    public static Schema schema(Context context, int version, int visitId) {
        Realm realm = Realm.getInstance(context);
        return realm.where(Schema.class)
                .equalTo("schemaVersion", version)
                .equalTo("idVisit", visitId)
                .findFirst();
    }

    public static void deleteUsers(Context context) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        RealmResults<User> users = realm.where(User.class).findAll();
        users.clear();
        realm.commitTransaction();
        realm.close();
    }


    public static RealmList<Visit> visits(Context context, String moduleName) {
        Module module = module(context, moduleName);
        RealmList<Visit> visits = new RealmList<>();
        if (module != null) {
            for(Visit visit : module.getVisits()){
                if(!visit.isVisited())
                    visits.add(visit);
            }
        }
        return visits;
    }

    public static RealmList<Visit> visitsDone(Context context, String moduleName) {
        Module module = module(context, moduleName);
        RealmList<Visit> visits = new RealmList<>();
        if (module != null) {
            for(Visit visit : module.getVisits()){
                if(visit.isVisited())
                    visits.add(visit);
            }
        }
        return visits;
    }

    public static Module module(Context context, String moduleName) {
        WorkPlan workPlan = Dao.workPlan(context);
        for(Module module : workPlan.getModules()){
            if (module.getTitle().equals(moduleName))
                return module;
        }
        return null;
    }
}
