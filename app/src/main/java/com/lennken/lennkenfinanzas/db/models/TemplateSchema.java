package com.lennken.lennkenfinanzas.db.models;

import android.content.Context;

import com.google.gson.Gson;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class TemplateSchema extends RealmObject{

    @PrimaryKey
    private String uuid;
    private int schemaVersion;
    private Schema schema;

    public TemplateSchema(){
        uuid = UUID.randomUUID().toString();
    }

    public static class Builder{

        private Context context;

        public Builder(Context context){
            this.context = context;
        }

        public Schema newSchema(int version){
            Realm realm = Realm.getInstance(context);
            TemplateSchema templateSchema = realm.where(TemplateSchema.class)
                    .equalTo("schemaVersion", version)
                    .findFirst();
            if(templateSchema == null) {
                return null;
            }else{
                Schema from = templateSchema.getSchema();
                Schema to = new Schema();
                to.setSchemaVersion(from.getSchemaVersion());
                RealmList<Survey> surveys = new RealmList<>();
                for(Survey fromSurvey : from.getSurveys()){
                    Survey toSurvey = new Survey();
                    toSurvey.setIdSurvey(fromSurvey.getIdSurvey());
                    toSurvey.setRequired(fromSurvey.isRequired());
                    toSurvey.setTitle(fromSurvey.getTitle());
                    RealmList<Question> questions = new RealmList<>();
                    for(Question fromQuestion : fromSurvey.getQuestions()){
                        Question toQuestion = new Question();
                        toQuestion.setIdQuestion(fromQuestion.getIdQuestion());
                        toQuestion.setPosition(fromQuestion.getPosition());
                        toQuestion.setQuestion(fromQuestion.getQuestion());
                        toQuestion.setQuestionType(fromQuestion.getQuestionType());
                        toQuestion.setRequired(fromQuestion.isRequired());
                        RealmList<Answer> answers = new RealmList<>();
                        for(Answer fromAnswer : fromQuestion.getAnswers()){
                            Answer toAnswer = new Answer();
                            toAnswer.setAnswerId(fromAnswer.getAnswerId());
                            toAnswer.setTitle(fromAnswer.getTitle());
                            toAnswer.setVisible(fromAnswer.isVisible());
                            toAnswer.setType(fromAnswer.getType());
                            answers.add(toAnswer);
                        }
                        toQuestion.setAnswers(answers);
                        questions.add(toQuestion);
                    }
                    toSurvey.setQuestions(questions);
                    surveys.add(toSurvey);
                }
                to.setSurveys(surveys);
                return to;
            }
        }
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(int schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public Schema getSchema() {
        return schema;
    }

    public void setSchema(Schema schema) {
        this.schema = schema;
    }
}
