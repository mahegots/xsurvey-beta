package com.lennken.lennkenfinanzas.db.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class Module extends RealmObject{

    @PrimaryKey
    private String uuid;
    private String title;
    private int schemaVersion;
    private RealmList<Visit> visits = new RealmList<>();
    private int idModule;

    public Module(){
        uuid = UUID.randomUUID().toString();
    }
    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The schemaVersion
     */
    public int getSchemaVersion() {
        return schemaVersion;
    }

    /**
     *
     * @param schemaVersion
     * The schemaVersion
     */
    public void setSchemaVersion(int schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    /**
     *
     * @return
     * The visits
     */
    public RealmList<Visit> getVisits() {
        return visits;
    }

    /**
     *
     * @param visits
     * The visits
     */
    public void setVisits(RealmList<Visit> visits) {
        this.visits = visits;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getIdModule() {
        return idModule;
    }

    public void setIdModule(int idModule) {
        this.idModule = idModule;
    }
}