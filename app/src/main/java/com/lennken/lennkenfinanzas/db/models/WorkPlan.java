package com.lennken.lennkenfinanzas.db.models;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class WorkPlan extends RealmObject{

    @PrimaryKey
    private String uuid;
    private int id;
    private int clientCode;
    private RealmList<Module> modules = new RealmList<>();

    public WorkPlan(){
        uuid = UUID.randomUUID().toString();
    }
    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The clientCode
     */
    public int getClientCode() {
        return clientCode;
    }

    /**
     *
     * @param clientCode
     * The clientCode
     */
    public void setClientCode(int clientCode) {
        this.clientCode = clientCode;
    }

    /**
     *
     * @return
     * The modules
     */
    public RealmList<Module> getModules() {
        return modules;
    }

    /**
     *
     * @param modules
     * The modules
     */
    public void setModules(RealmList<Module> modules) {
        this.modules= modules;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
