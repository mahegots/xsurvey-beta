package com.lennken.lennkenfinanzas.db.models;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class Response extends RealmObject {

    @PrimaryKey
    private String uuid;
    private String response;
    private int idResponse;

    public Response() {
        this.uuid = UUID.randomUUID().toString();
    }

    public Response(String response, int idResponse){
        this.uuid = UUID.randomUUID().toString();
        this.response = response;
        this.idResponse = idResponse;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getIdResponse() {
        return idResponse;
    }

    public void setIdResponse(int idResponse) {
        this.idResponse = idResponse;
    }
}
