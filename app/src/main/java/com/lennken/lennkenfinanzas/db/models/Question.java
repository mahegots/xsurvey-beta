package com.lennken.lennkenfinanzas.db.models;

import com.google.gson.annotations.Expose;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class Question extends RealmObject {

    @PrimaryKey
    private String uuid;
    @Expose(serialize = false)
    private RealmList<Answer> answers;
    private String question;
    @Expose(serialize = false)
    private int questionType;
    private int position;
    @Expose(serialize = false)
    private boolean required;
    private int idQuestion;
    private String response;
    private int idResponse;
    @Expose(deserialize = false, serialize = false)
    private int arrayPosition;
    private RealmList<Response> responseArray;

    public Question() {
        this.uuid = UUID.randomUUID().toString();
        answers = new RealmList<>();
        responseArray = new RealmList<>();
        arrayPosition = 0;
        response = "";
        required = false;
        idResponse = -1;
    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public RealmList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(RealmList<Answer> answers) {
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    public int getIdResponse() {
        return idResponse;
    }

    public void setIdResponse(int idResponse) {
        this.idResponse = idResponse;
    }

    public RealmList<Response> getResponseArray() {
        return responseArray;
    }

    public void setResponseArray(RealmList<Response> responseArray) {
        this.responseArray = responseArray;
    }

    public int getArrayPosition() {
        return arrayPosition;
    }

    public void setArrayPosition(int arrayPosition) {
        this.arrayPosition = arrayPosition;
    }
}
