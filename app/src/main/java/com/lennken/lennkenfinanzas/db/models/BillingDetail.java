package com.lennken.lennkenfinanzas.db.models;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/23/15.
 * Lennken Finanzas - danielgarcia
 */
public class BillingDetail extends RealmObject{

    @PrimaryKey
    private String uuid;
    private String concept;
    private double amount;
    private int noVisit;

    public BillingDetail(){
        uuid = UUID.randomUUID().toString();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getNoVisit() {
        return noVisit;
    }

    public void setNoVisit(int noVisit) {
        this.noVisit = noVisit;
    }

}
