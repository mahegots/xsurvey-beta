package com.lennken.lennkenfinanzas.db.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/20/15.
 * Lennken Finanzas - danielgarcia
 */
public class Visit extends RealmObject{

    @PrimaryKey
    private String uuid;
    private String legalEntity;
    private String clientName;
    private String clientContact;
    private String addressOne;
    private String addressTwo;
    private String state;
    private String city;
    private String telephoneNumber;
    private String comments;
    private String latitude;
    private String longitude;
    private double amount;
    private String schedule;
    private int idVisit;
    private int visitOrder;
    private String statusValue;
    private String statusDescription;
    private RealmList<BillingDetail> billingDetailList;
    @Expose(deserialize = false, serialize = false)
    private boolean visited;

    public Visit(){
        uuid = UUID.randomUUID().toString();
        billingDetailList = new RealmList<>();
        visited = false;
    }
    /**
     * @return The legalEntity
     */
    public String getLegalEntity() {
        return legalEntity;
    }

    /**
     * @param legalEntity The legalEntity
     */
    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    /**
     * @return The clientName
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * @param clientName The clientName
     */
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * @return The clientContact
     */
    public String getClientContact() {
        return clientContact;
    }

    /**
     * @param clientContact The clientContact
     */
    public void setClientContact(String clientContact) {
        this.clientContact = clientContact;
    }

    /**
     * @return The addressOne
     */
    public String getAddressOne() {
        return addressOne;
    }

    /**
     * @param addressOne The addressOne
     */
    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    /**
     * @return The addressTwo
     */
    public String getAddressTwo() {
        return addressTwo;
    }

    /**
     * @param addressTwo The addressTwo
     */
    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The telephoneNumber
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * @param telephoneNumber The telephoneNumber
     */
    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * @return The comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments The comments
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public int getIdVisit() {
        return idVisit;
    }

    public void setIdVisit(int idVisit) {
        this.idVisit = idVisit;
    }


    public int getVisitOrder() {
        return visitOrder;
    }

    public void setVisitOrder(int visitOrder) {
        this.visitOrder = visitOrder;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public RealmList<BillingDetail> getBillingDetailList() {
        return billingDetailList;
    }

    public void setBillingDetailList(RealmList<BillingDetail> billingDetailList) {
        this.billingDetailList = billingDetailList;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
