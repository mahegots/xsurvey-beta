package com.lennken.lennkenfinanzas.db.models;

import com.google.gson.annotations.Expose;
import com.lennken.lennkenfinanzas.xsurvey.enums.SchemaStatus;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 8/21/15.
 * Lennken Finanzas - danielgarcia
 */
public class Schema extends RealmObject {

    @PrimaryKey
    private String uuid;
    private RealmList<Survey> surveys;
    @Expose(deserialize = false, serialize = false)
    private Visit visit;
    private int idModule;
    private int idWorkPlan;
    private int schemaVersion;
    private int idVisit;
    private int mobileType;
    private String username;
    private String pushId;
    private int status;
    private double latitude;
    private double longitude;

    public Schema() {
        this.uuid = UUID.randomUUID().toString();
        surveys = new RealmList<>();
        status = SchemaStatus.INCOMPLETE;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public RealmList<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(RealmList<Survey> surveys) {
        this.surveys = surveys;
    }

    public int getIdModule() {
        return idModule;
    }

    public void setIdModule(int idModule) {
        this.idModule = idModule;
    }

    public int getIdWorkPlan() {
        return idWorkPlan;
    }

    public void setIdWorkPlan(int idWorkPlan) {
        this.idWorkPlan = idWorkPlan;
    }

    public int getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(int schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public int getIdVisit() {
        return idVisit;
    }

    public void setIdVisit(int idVisit) {
        this.idVisit = idVisit;
    }

    public int getMobileType() {
        return mobileType;
    }

    public void setMobileType(int mobileType) {
        this.mobileType = mobileType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }
}
